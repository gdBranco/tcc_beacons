\select@language {brazil}
\contentsline {section}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{3}
\contentsline {subsection}{\numberline {1.1}Objetivos}{3}
\contentsline {subsection}{\numberline {1.2}Metodologia da pesquisa}{3}
\contentsline {subsection}{\numberline {1.3}Resultados}{4}
\contentsline {subsection}{\numberline {1.4}Estrutura do documento}{4}
\contentsline {section}{\numberline {2}Fundamenta\IeC {\c c}\IeC {\~a}o te\IeC {\'o}rica}{5}
\contentsline {subsection}{\numberline {2.1}\textit {Physical Web}}{5}
\contentsline {subsection}{\numberline {2.2}Tecnologias para comunica\IeC {\c c}\IeC {\~a}o sem fio}{6}
\contentsline {subsubsection}{\numberline {2.2.1}IEEE 802.15.4}{6}
\contentsline {subsubsection}{\numberline {2.2.2}Bluetooth}{7}
\contentsline {subsection}{\numberline {2.3}BLE Beacons e Protocolos de encapsulamento de dados}{9}
\contentsline {subsubsection}{\numberline {2.3.1}BLE Beacons}{9}
\contentsline {subsubsection}{\numberline {2.3.2}Protocolos de encapsulamento de dados}{10}
\contentsline {subsection}{\numberline {2.4}Tecnologias para localiza\IeC {\c c}\IeC {\~a}o e posicionamento espacial interno}{12}
\contentsline {subsubsection}{\numberline {2.4.1}\textit {Fingerprinting}}{13}
\contentsline {subsubsection}{\numberline {2.4.2}Calculando a dist\IeC {\^a}ncia com sinais de frequ\IeC {\^e}ncia}{13}
\contentsline {subsubsection}{\numberline {2.4.3}Trilatera\IeC {\c c}\IeC {\~a}o}{14}
\contentsline {section}{\numberline {3}Trabalhos relacionados}{16}
