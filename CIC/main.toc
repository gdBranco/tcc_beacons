\select@language {american}
\select@language {american}
\select@language {brazil}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Problema}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Objetivo Geral}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Objetivos Espec\IeC {\'\i }ficos}{4}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Metodologia da Pesquisa}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Estrutura do Trabalho}{5}{section.1.4}
\contentsline {chapter}{\numberline {2}Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Pilha TCP/IP}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Camada de Aplica\IeC {\c c}\IeC {\~a}o}{9}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Camada de Transporte}{9}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Camada de Rede}{10}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Camada de Enlace}{10}{subsection.2.1.4}
\contentsline {subsubsection}{Endere\IeC {\c c}os MAC}{11}{section*.10}
\contentsline {subsubsection}{Detec\IeC {\c c}\IeC {\~a}o de Erros}{11}{section*.11}
\contentsline {subsubsection}{Cyclic Redundancy Check}{12}{section*.12}
\contentsline {subsection}{\numberline {2.1.5}Camada F\IeC {\'\i }sica}{12}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}Tecnologias para Comunica\IeC {\c c}\IeC {\~a}o Sem Fio}{13}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}\acrshort {IEEE} 802.15}{14}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}\acrshort {IEEE} 802.15.1 - \textit {Bluetooth}}{14}{subsection.2.2.2}
\contentsline {subsubsection}{\textbf {\textit {Bluetooth Low-Energy}}}{15}{section*.13}
\contentsline {subsection}{\numberline {2.2.3}\acrshort {IEEE} 802.15.4}{17}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}\textit {Physical Web}}{18}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}\textit {Beacons}}{19}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Protocolos de Encapsulamento de Informa\IeC {\c c}\IeC {\~o}es}{20}{subsection.2.3.2}
\contentsline {subsubsection}{iBeacon}{20}{section*.14}
\contentsline {subsubsection}{Eddystone}{20}{section*.15}
\contentsline {section}{\numberline {2.4}Discuss\IeC {\~a}o}{22}{section.2.4}
\contentsline {chapter}{\numberline {3}Mecanismos para Localiza\IeC {\c c}\IeC {\~a}o de Dispositivos BLE}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}\textit {Fingerprinting}}{25}{section.3.1}
\contentsline {section}{\numberline {3.2}\textit {Time Difference of Arrival} e \textit {Time of Arrival}}{25}{section.3.2}
\contentsline {section}{\numberline {3.3}\textit {Angle of Arrival}}{26}{section.3.3}
\contentsline {section}{\numberline {3.4}Modelos de Propaga\IeC {\c c}\IeC {\~a}o do Sinal}{26}{section.3.4}
\contentsline {section}{\numberline {3.5}Ru\IeC {\'\i }do do RSSI}{29}{section.3.5}
\contentsline {section}{\numberline {3.6}Trilatera\IeC {\c c}\IeC {\~a}o}{30}{section.3.6}
\contentsline {section}{\numberline {3.7}Estado da Arte}{31}{section.3.7}
\contentsline {section}{\numberline {3.8}Discuss\IeC {\~a}o}{36}{section.3.8}
\contentsline {chapter}{\numberline {4}Avalia\IeC {\c c}\IeC {\~a}o de T\IeC {\'e}cnicas de Localiza\IeC {\c c}\IeC {\~a}o de Dispositivos BLE}{37}{chapter.4}
\contentsline {section}{\numberline {4.1}Metodologia de Coleta de Informa\IeC {\c c}\IeC {\~o}es}{37}{section.4.1}
\contentsline {section}{\numberline {4.2}Flutua\IeC {\c c}\IeC {\~a}o da Intensidade do Sinal}{41}{section.4.2}
\contentsline {section}{\numberline {4.3}An\IeC {\'a}lise de Filtros}{42}{section.4.3}
\contentsline {section}{\numberline {4.4}Ambientes de Teste}{46}{section.4.4}
\contentsline {section}{\numberline {4.5}Resultados}{47}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Calibra\IeC {\c c}\IeC {\~a}o dos Modelos de Propaga\IeC {\c c}\IeC {\~a}o}{48}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Calibra\IeC {\c c}\IeC {\~a}o da Converg\IeC {\^e}ncia de \acrshort {RSSI}}{48}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Estimativa de Dist\IeC {\^a}ncia}{50}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}Posicionamento}{53}{subsection.4.5.4}
\contentsline {section}{\numberline {4.6}Prova de Conceito}{54}{section.4.6}
\contentsline {chapter}{\numberline {5}Conclus\IeC {\~a}o}{58}{chapter.5}
\contentsline {subsubsection}{Trabalhos Futuros}{59}{section*.16}
\contentsline {chapter}{Refer\^encias}{60}{section*.17}
\select@language {american}
\select@language {american}
\select@language {american}
\select@language {brazil}
