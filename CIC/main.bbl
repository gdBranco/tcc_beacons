\begin{thebibliography}{10}
  \providebibliographyfont{name}{}%
  \providebibliographyfont{lastname}{}%
  \providebibliographyfont{title}{\emph}%
  \providebibliographyfont{jtitle}{\btxtitlefont}%
  \providebibliographyfont{etal}{\emph}%
  \providebibliographyfont{journal}{}%
  \providebibliographyfont{volume}{}%
  \providebibliographyfont{ISBN}{\MakeUppercase}%
  \providebibliographyfont{ISSN}{\MakeUppercase}%
  \providebibliographyfont{url}{\url}%
  \providebibliographyfont{numeral}{}%
  \expandafter\btxselectlanguage\expandafter {\btxfallbacklanguage}

\expandafter\btxselectlanguage\expandafter {\btxfallbacklanguage}
\bibitem {2015:EnablingIot}
\btxnamefont {\btxlastnamefont {Want}, Roy}, \btxnamefont
  {Bill\btxfnamespacelong N. \btxlastnamefont {Schilit}}, \btxandcomma {}
  \btxandlong {}\ \btxnamefont {Scott \btxlastnamefont
  {Jenson}}\btxauthorcolon\ \btxjtitlefont {\btxifchangecase {Enabling internet
  of things}{Enabling Internet of Things}}.
\newblock \btxjournalfont {IEEE Computer Society}, 2015.

\bibitem {2016:turco}
\btxnamefont {\btxlastnamefont {Vargas}, Milan\btxfnamespacelong
  Herrera}\btxauthorcolon\ \btxjtitlefont {\btxifchangecase {Indoor navigation
  using bluetooth low energy beacons}{Indoor navigation using bluetooth low
  energy beacons}}.
\newblock \btxjournalfont {Turkey University}, 2016.

\bibitem {2015:smartcities}
\btxnamefont {\btxlastnamefont {Namiot}, Dmitry} \btxandlong {}\ \btxnamefont
  {Manfed \btxlastnamefont {Sneps-Sneppe}}\btxauthorcolon\ \btxjtitlefont
  {\btxifchangecase {The physical web in smart cities}{The Physical Web in
  Smart Cities}}.
\newblock \btxjournalfont {IEEE}, 2015.

\bibitem {2000:webpresence}
\btxnamefont {\btxlastnamefont {Kindberg}, Tim}, \btxnamefont {John
  \btxlastnamefont {Barton}}, \btxnamefont {Jeff \btxlastnamefont {Morgan}},
  \btxnamefont {Gene \btxlastnamefont {Becker}}, \btxnamefont {Debbie
  \btxlastnamefont {Caswell}}, \btxnamefont {Philippe \btxlastnamefont
  {Debaty}}, \btxnamefont {Gita \btxlastnamefont {Gopal}}, \btxnamefont {Marcos
  \btxlastnamefont {Frid}}, \btxnamefont {Venky \btxlastnamefont {Krishnan}},
  \btxnamefont {Howard \btxlastnamefont {Morris}}, \btxnamefont {John
  \btxlastnamefont {Schettino}}, \btxnamefont {Bill \btxlastnamefont
  {Serra}}\btxandcomma {} \btxandlong {}\ \btxnamefont {Mirjana
  \btxlastnamefont {Spasojevic}}\btxauthorcolon\ \btxjtitlefont
  {\btxifchangecase {People, places, things: Web presence for the real
  world}{People, Places, Things: Web Presence for the Real World}}.
\newblock \btxjournalfont {IEEE Computer Society}, 2000.

\bibitem {PhyGoogle}
\btxnamefont {\btxlastnamefont {Google}}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Physical web}{Physical Web}}.
\newblock https://github.com/google/physical-web, acessado em julho 2016.

\bibitem {1999:Triggers}
\btxnamefont {\btxlastnamefont {C}, Andrew}, \btxnamefont {\btxlastnamefont
  {Huang}}, \btxnamefont {Benjamin\btxfnamespacelong C. \btxlastnamefont
  {Ling}}\btxandcomma {} \btxandlong {}\ \btxnamefont {Shankar \btxlastnamefont
  {Ponnekanti}}\btxauthorcolon\ \btxjtitlefont {\btxifchangecase {Pervasive
  computing: What is it good for?}{Pervasive Computing: What Is It Good For?}}
\newblock \btxjournalfont {International Workshop on Data Engineering for
  Wireless and Mobile Access}, 1999.

\bibitem {BKON}
\btxnamefont {\btxlastnamefont {BKON}}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Bkon}{BKON}}.
\newblock https://bkon.com/, Acessado em março 2016.

\bibitem {Levine:2016}
\btxnamefont {\btxlastnamefont {Levine}, Barry}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {With the physical web, you become the search engine}{With
  The Physical Web, You Become The Search Engine}}.
\newblock
  http://marketingland.com/with-the-physical-web-you-become-the-search-engine-165338,
  acessado em julho 2016, 2016.

\bibitem {2012:context_browser}
\btxnamefont {\btxlastnamefont {Namiot}, Dmitry}\btxauthorcolon\ \btxjtitlefont
  {\btxifchangecase {Context-aware browsing – a practical
  approach}{Context-aware Browsing – a practical approach}}.
\newblock \btxjournalfont {IEEE}, 2012.

\bibitem {Brookwood:2016}
\btxnamefont {\btxlastnamefont {Evans}, Katie}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Beacons go to school}{Beacons go to school}}.
\newblock https://www.mobilestrategies360.com/ 2016/01/14/beacons-go-school,
  Acessado julho 2016, 2016.

\bibitem {eddystone_PW:2016}
\btxnamefont {\btxlastnamefont {Trnka}, Tomas}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Eddystone beacon technology and the physical
  web}{Eddystone beacon technology and the Physical Web}}.
\newblock
  https://mobiforge.com/design-development/eddystone-beacon-technology-and-the-physical-web,
  acessado em julho de 2016.

\bibitem {ABI}
\btxnamefont {\btxlastnamefont {Research}, ABI}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {New ble beacons coverage}{New BLE Beacons Coverage}}.
\newblock https://www.abiresearch.com/pages/ble-beacons/, Acessado em março
  2016, 2016.

\bibitem {Eddystone}
\btxnamefont {\btxlastnamefont {Google}}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Eddystone format}{Eddystone Format}}.
\newblock https://developers.google.com/beacons/eddystone, Acessado em março
  2016, 2016.

\bibitem {ibeacon}
\btxnamefont {\btxlastnamefont {Apple}}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {ibeacon}{iBeacon}}.
\newblock https://developer.apple.com/ibeacon/, Acessado em março 2016.

\bibitem {2011:Fingerlateration}
\btxnamefont {\btxlastnamefont {Subhan}, Fazli}, \btxnamefont {Azat
  \btxlastnamefont {Rozyyev}}, \btxnamefont {Halabi \btxlastnamefont
  {Hasbullah}}\btxandcomma {} \btxandlong {}\ \btxnamefont
  {Sheikh\btxfnamespacelong Tahir \btxlastnamefont {Bakhsh}}\btxauthorcolon\
  \btxjtitlefont {\btxifchangecase {Indoor positioning in bluetooth networks
  using fingerprinting and lateration approach}{Indoor Positioning in Bluetooth
  Networks using Fingerprinting and Lateration approach}}.
\newblock \btxjournalfont {IEEE}, 2011.

\bibitem {Combined:2013}
\btxnamefont {\btxlastnamefont {Galván-Tejada}, Carlos\btxfnamespacelong E.},
  \btxnamefont {José\btxfnamespacelong C. \btxlastnamefont
  {Carrasco-Jiménez}}\btxandcomma {} \btxandlong {}\ \btxnamefont
  {Ramon\btxfnamespacelong F. \btxlastnamefont {Brenna}}\btxauthorcolon\
  \btxjtitlefont {\btxifchangecase {Bluetooth-wifi based combined positioning
  algorithm, implementation and experimental evaluation}{Bluetooth-WiFi based
  combined positioning algorithm, implementation and experimental evaluation}}.
\newblock \btxjournalfont {The 2013 Iberoamerican Conference on Electronics
  Engineering and Computer Science}, 2013.

\bibitem {Comparison:2011}
\btxnamefont {\btxlastnamefont {Dalce}, Rejane}, \btxnamefont {Thierry
  \btxlastnamefont {Val}}\btxandcomma {} \btxandlong {}\ \btxnamefont
  {Adrien\btxfnamespacelong Van\btxfnamespacelong Den \btxlastnamefont
  {Bossche}}\btxauthorcolon\ \btxjtitlefont {\btxifchangecase {Comparison of
  indoor localization systems based on wireless communications}{Comparison of
  Indoor Localization Systems Based on Wireless Communications}}.
\newblock \btxjournalfont {Wireless Engineering and Technology}, 2011.

\bibitem {kurose}
\btxnamefont {\btxlastnamefont {Kurose}, James\btxfnamespacelong F.}
  \btxandlong {}\ \btxnamefont {Keith\btxfnamespacelong W. \btxlastnamefont
  {Ross}}\btxauthorcolon\ \btxtitlefont {Computer Networking A Top-Down
  Approach}.
\newblock \btxpublisherfont {Pearson}, 2013.

\bibitem {TanenbaumBook}
\btxnamefont {\btxlastnamefont {Tanenbaum}} \btxandlong {}\ \btxnamefont
  {\btxlastnamefont {Wheterall}}\btxauthorcolon\ \btxtitlefont {Computer
  Networks}.
\newblock \btxpublisherfont {Pearson}, 2011.

\bibitem {garcia}
\btxnamefont {\btxlastnamefont {Leon-Garcia}, Alberto} \btxandlong {}\
  \btxnamefont {Indra \btxlastnamefont {Widjaja}}\btxauthorcolon\ \btxtitlefont
  {Communication Networks: Fundamental Concepts and Key Archictectures}.
\newblock \btxpublisherfont {McGraw-Hill}, 2000.

\bibitem {IEEE802.15}
\btxnamefont {\btxlastnamefont {IEEE}}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Ieee 802.15 working group for wireless specialty networks
  (wsn)}{IEEE 802.15 Working Group for Wireless Specialty Networks (WSN)}}.
\newblock http://www.ieee802.org/15/, 2017.

\bibitem {IEEE802.15.4}
\btxnamefont {\btxlastnamefont {IEEE}}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Ieee 802.15 wpan™ task group 4 (tg4)}{IEEE 802.15
  WPAN™ Task Group 4 (TG4)}}.
\newblock http://www.ieee802.org/15/pub/TG4.html.

\bibitem {IEEE802.15.1}
\btxnamefont {\btxlastnamefont {IEEE}}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Ieee 802.15 wpan task group 1 (tg1)}{IEEE 802.15 WPAN Task
  Group 1 (TG1)}}.
\newblock http://www.ieee802.org/15/pub/TG1.html, 2017.

\bibitem {bsig}
\btxnamefont {\btxlastnamefont {Group}, Bluetooth\btxfnamespacelong
  Special\btxfnamespacelong Interest}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Bluetooth}{Bluetooth}}.
\newblock https://www.bluetooth.com/, 2017.

\bibitem {BluetoothBook}
\btxnamefont {\btxlastnamefont {Bray}, Jennifer} \btxandlong {}\ \btxnamefont
  {Charles\btxfnamespacelong F. \btxlastnamefont {Sturman}}\btxauthorcolon\
  \btxtitlefont {Bluetooth : Connect Without Cables}.
\newblock \btxpublisherfont {Pearson Education}, 2001.

\bibitem {TIReport}
\btxnamefont {\btxlastnamefont {Lindh}, Joakim}\btxauthorcolon\ \btxjtitlefont
  {\btxifchangecase {Bluetooth low energy beacons application report}{Bluetooth
  Low Energy Beacons Application Report}}.
\newblock \btxjournalfont {Texas Instruments}, 2016.

\bibitem {2005:802.15.4}
\btxnamefont {\btxlastnamefont {Gutierrez}, José\btxfnamespacelong
  A.}\btxauthorcolon\ \btxtitlefont {\btxifchangecase {Ieee 802.15.4 enabling
  pervasive wireless sensor networks}{IEEE 802.15.4 Enabling Pervasive Wireless
  Sensor Networks}}.
\newblock
  https://people.eecs.berkeley.edu/~prabal/teaching/cs294-11-f05/slides/day21.pdf,
  acessado em julho de 2016.

\bibitem {2010:interference}
\btxnamefont {\btxlastnamefont {Liang}, Chieh\btxfnamespacelong
  Jan\btxfnamespacelong Mike}, \btxnamefont {Nissanka\btxfnamespacelong Bodhi
  \btxlastnamefont {Priyantha}}, \btxnamefont {Jie \btxlastnamefont
  {Liu}}\btxandcomma {} \btxandlong {}\ \btxnamefont {Andreas \btxlastnamefont
  {Terzis}}\btxauthorcolon\ \btxjtitlefont {\btxifchangecase {Surviving wi-fi
  interference in low power zigbee networks}{Surviving Wi-Fi Interference in
  Low Power ZigBee Networks}}.
\newblock \btxjournalfont {SenSys 2010 ACM}, 2010.

\bibitem {ZBLE}
\btxnamefont {\btxlastnamefont {Rivero}, Armando}\btxauthorcolon\
  \btxjtitlefont {\btxifchangecase {Zigbee and bluetooth: Low energy and
  smartphone integration}{ZigBee and Bluetooth: Low Energy and Smartphone
  Integration}}.
\newblock \btxjournalfont {Universit`a della Svizzera Italiana ALaRI
  Institute}, 2013.

\bibitem {ibeacon:2014}
\btxnamefont {\btxlastnamefont {Apple}}\btxauthorcolon\ \btxtitlefont {Getting
  Started with iBeacon}.
\newblock Apple, \btxprintmonthyear{.}{jun}{2014}{long}.
\newblock {\latintext
  \btxurlfont{https://developer.apple.com/ibeacon/Getting-Started-with-iBeacon.pdf}}.

\bibitem {LOSComparison:2008}
\btxnamefont {\btxlastnamefont {Shen}, Guowei}, \btxnamefont {Rudolf
  \btxlastnamefont {Zetik}}\btxandcomma {} \btxandlong {}\ \btxnamefont
  {Reiner\btxfnamespacelong S. \btxlastnamefont {Thomä}}\btxauthorcolon\
  \btxjtitlefont {\btxifchangecase {Performance comparison of toa and tdoa
  based location estimation algorithms in los environment}{Performance
  Comparison of TOA and TDOA Based Location Estimation Algorithms in LOS
  Environment}}.
\newblock \btxjournalfont {WPNC(Workshop on positioning, navigation and
  communication)}, 2008.

\bibitem {AccuracyTOA:2012}
\btxnamefont {\btxlastnamefont {Kaune}, Regina}\btxauthorcolon\ \btxjtitlefont
  {\btxifchangecase {Accuracy studies for tdoa and toa localization}{Accuracy
  Studies for TDOA and TOA Localization}}.
\newblock \btxjournalfont {Fraunhofer FKIE/University of Bonn}, 2012.

\bibitem {GPSperformance}
\btxnamefont {\btxlastnamefont {Hughes}, William\btxfnamespacelong
  J.}\btxauthorcolon\ \btxjtitlefont {\btxifchangecase {Global positioning
  system (gps) standard positioning service (sps) performance analysis
  report}{Global Positioning System (GPS) Standard Positioning Service (SPS)
  Performance Analysis Report}}.
\newblock \btxjournalfont {Federal Aviation Administration}, 2013.

\bibitem {Phone_Limitations:2011}
\btxnamefont {\btxlastnamefont {Blank}, Aaron}\btxauthorcolon\ \btxjtitlefont
  {\btxifchangecase {The limitations and admissibility of using historical
  cellular site data to track the location of a cellular phone}{The Limitations
  and Admissibility of Using Historical Cellular Site Data to Track the
  Location of a Cellular Phone}}.
\newblock \btxjournalfont {Richmond Journal of Law \& Technology}, 2011.

\bibitem {Recursive_estimation:2001}
\btxnamefont {\btxlastnamefont {Albowicz}, Joe}, \btxnamefont {Alvin
  \btxlastnamefont {Chen}}\btxandcomma {} \btxandlong {}\ \btxnamefont {Lixia
  \btxlastnamefont {Zhang}}\btxauthorcolon\ \btxjtitlefont {\btxifchangecase
  {Recursive position estimation in sensor networks}{Recursive Position
  Estimation in Sensor Networks}}.
\newblock \btxjournalfont {IEEE}, 2001.

\bibitem {navstar}
\btxnamefont {\btxlastnamefont {Sturdevant}, Rick\btxfnamespacelong
  W.}\btxauthorcolon\ \btxtitlefont {Societal Impact of Spaceflight : Chapter
  17}.
\newblock \btxpublisherfont {NASA}, 2007.

\bibitem {positioning:2013}
\btxnamefont {\btxlastnamefont {Wang}, Yapeng}, \btxnamefont {Xu
  \btxlastnamefont {Yang}}, \btxnamefont {Yutian \btxlastnamefont {Zhao}},
  \btxnamefont {Yue \btxlastnamefont {Liu}}\btxandcomma {} \btxandlong {}\
  \btxnamefont {Laurie \btxlastnamefont {Cuthbert}}\btxauthorcolon\
  \btxjtitlefont {\btxifchangecase {Bluetooth positioning using rssi and
  triangulation methods}{Bluetooth Positioning using RSSI and Triangulation
  Methods}}.
\newblock \btxjournalfont {IEEE Consumer Communications and Networking
  Conference(CCNC)}, \btxpageslong {}\ 837--842, 2013.

\bibitem {2015:Dfilter}
\btxnamefont {\btxlastnamefont {Viswanathan}, Subha} \btxandlong {}\
  \btxnamefont {Sreedevi \btxlastnamefont {Srinivasan}}\btxauthorcolon\
  \btxjtitlefont {\btxifchangecase {Improved path loss prediction model for
  short range indoor positioning using bluetooth low energy}{Improved Path Loss
  Prediction Model for Short Range Indoor Positioning using Bluetooth Low
  Energy}}.
\newblock \btxjournalfont {IEEE}, 2015.

\bibitem {Calibration:2002}
\btxnamefont {\btxlastnamefont {Whitehouse}, Kamin} \btxandlong {}\
  \btxnamefont {David \btxlastnamefont {Culler}}\btxauthorcolon\ \btxjtitlefont
  {\btxifchangecase {Calibration as parameter estimation in sensor
  networks}{Calibration as Parameter Estimation in Sensor Networks}}.
\newblock \btxjournalfont {WSNA}, 2002.

\bibitem {glomoman}
\btxnamefont {\btxlastnamefont {Nuevo}, Jorge}\btxauthorcolon\ \btxtitlefont {A
  Comprehensiable GloMoSim Tutorial}.
\newblock \btxpublisherfont {Université Québec}, 2004.

\bibitem {Kalman_intro}
\btxnamefont {\btxlastnamefont {Welch}, Greg} \btxandlong {}\ \btxnamefont
  {Gary \btxlastnamefont {Bishop}}\btxauthorcolon\ \btxjtitlefont
  {\btxifchangecase {An introduction to the kalman filter}{An introduction to
  the Kalman Filter}}.
\newblock \btxjournalfont {University of North Carolina at Chapel Hill}, 2001.

\bibitem {2015:Kalman_explained}
\btxnamefont {\btxlastnamefont {Bulten}, Wouter}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Kalman filters explained: Removing noise from rssi
  signals}{Kalman filters explained: Removing noise from RSSI signals}}.
\newblock
  https://wouterbulten.nl/blog/tech/kalman-filters-explained-removing-noise-from-rssi-signals/,
  Acessado em junho 2016, 2015.

\bibitem {1999:GUIDE}
\btxnamefont {\btxlastnamefont {Davies}, N.}, \btxnamefont {K. \btxlastnamefont
  {Cheverst}}, \btxnamefont {K. \btxlastnamefont {Mitchell}}\btxandcomma {}
  \btxandlong {}\ \btxnamefont {A. \btxlastnamefont {Friday}}\btxauthorcolon\
  \btxjtitlefont {\btxifchangecase {`caches in the air': disseminating tourist
  information in the guide system}{`Caches in the air': disseminating tourist
  information in the GUIDE system}}.
\newblock \btxjournalfont {WMCSA'99}, 1999.

\bibitem {ble-arduino}
\btxnamefont {\btxlastnamefont {Deordica}, Bianca} \btxandlong {}\ \btxnamefont
  {Marian \btxlastnamefont {Alexandru}}\btxauthorcolon\ \btxjtitlefont
  {\btxifchangecase {Advertisement using bluetooth low energy}{Advertisement
  Using Bluetooth Low Energy}}.
\newblock \btxjournalfont {Review of the Air Force Academy}, 2014.

\bibitem {ocupacao-BLE}
\btxnamefont {\btxlastnamefont {Filippoupolitis}, Avgoustinos}, \btxnamefont
  {William \btxlastnamefont {Oliff}}\btxandcomma {} \btxandlong {}\
  \btxnamefont {George \btxlastnamefont {Loukas}}\btxauthorcolon\
  \btxjtitlefont {\btxifchangecase {Bluetooth low energy based occupancy
  detection for emergency management}{Bluetooth Low Energy based Occupancy
  Detection for Emergency Management}}.
\newblock \btxjournalfont {IEEE}, 2016.

\bibitem {KST}
\btxnamefont {\btxlastnamefont {KST}}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Kst}{KST}}.
\newblock https://kstechnologies.com/, Acessado em março 2016.

\bibitem {2014:Ion}
\btxnamefont {\btxlastnamefont {KS~Technologies}, Revision\btxfnamespacelong
  A}\btxauthorcolon\ \btxtitlefont {Device Specification: Ion},
  \btxprintmonthyear{.}{December}{2014}{long}.
\newblock {\latintext
  \btxurlfont{https://kstechnologies.com/wp-content/uploads/2015/04/Ion-Device-Specification-Rev-A.pdf}}.

\bibitem {2015:Particle}
\btxnamefont {\btxlastnamefont {KS~Technologies}, Revision\btxfnamespacelong
  B}\btxauthorcolon\ \btxtitlefont {Device Specification: Particle},
  \btxprintmonthyear{.}{April}{2015}{long}.
\newblock {\latintext
  \btxurlfont{https://kstechnologies.com/wp-content/uploads/2015/04/Particle-Device-Specification-Rev-B.pdf}}.

\bibitem {ZPLAYspec}
\btxnamefont {\btxlastnamefont {Lenovo}}\btxauthorcolon\ \btxtitlefont
  {\btxifchangecase {Moto z play xt1635-02 specification}{Moto Z Play XT1635-02
  Specification}}.
\newblock http://www.gsmarena.com/ motorola\_moto\_z\_play-8310.php, Acessado
  em setembro 2016, 2016.

\end{thebibliography}
