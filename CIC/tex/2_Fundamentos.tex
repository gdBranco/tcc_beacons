A fundamentação teórica visa nortear, objetivando a revisão de conceitos e de técnicas aplicados em sua elaboração. Assim, este capítulo intenta elucidar os diversos tópicos abordados nesta pesquisa, para situar as questões e limitações da \textit{Physical Web}, como o funcionamento da pilha \acrfull{TCP}/\acrfull{IP}, das tecnologias para comunicação sem fio, dos protocolos de encapsulamento de informação e dos \textit{beacons}.

\section{Pilha TCP/IP} \label{TCP/IP}
O texto referente a esta seção tem como base os autores Kurose e Ross \cite{kurose}, Tanenbaum \cite{TanenbaumBook} e Leon-Garcia e Widjaja \cite{garcia}. Segundo Kurose e Ross \cite{kurose}, a Internet constitui um sistema extremamente complexo e multifacetado. Assim, na comunicação entre dois computadores (mesmo com \textit{hardware} e \textit{software} diferentes), foram instituídos conjuntos de padrões e normas, denominados, no ambiente de redes, de protocolos.

Os protocolos de rede se encontram, geralmente, projetados e organizados em uma estrutura de camadas, onde cada camada se torna responsável por promover e executar algumas ações específicas, fornecendo algum serviço à camada superior. Isto ocorre devido ao modelo ter como base o conceito de encapsulamento, ou seja, mensagens pertencentes a outra camada não podem ser modificadas, apenas encapsuladas junto às mensagens da camada atual. Desse modo, uma camada que esteja localizada acima de outra, não precisa conhecer o seu funcionamento interno, somente saber os serviços que aquela provê. Isso facilita a modularidade, pois quando se altera uma camada, precisa-se simplesmente assegurar que os serviços fornecidos antes por ela continuarão à disposição da camada superior, garantindo que tudo prossiga funcionando perfeitamente sem a necessidade de realizar alterações nas demais camadas.

Dois importantes protocolos se destacam nesta tarefa, o \acrshort{TCP} e o \acrshort{IP}, constituindo, respectivamente, a camada de transporte de serviço com conexão e a camada de rede, formando assim a pilha \acrshort{TCP}/\acrshort{IP}, projetada em uma arquitetura na qual as diversas camadas de software se comunicam somente com as camadas que se encontram imediatamente acima e abaixo.

A Figura \ref{fig:msg-tcpip} exemplifica, de uma maneira geral, a transmissão de uma mensagem entre dois usuários e ilustra o conceito de encapsulamento. Mostra o caminho que uma mensagem percorre, passando por cada camada desde a origem até o destino. Após ser gerada na camada de aplicação pelo remetente, a mensagem será repassada à camada inferior, no caso, a de transporte, a qual insere um cabeçalho, ou seja, informações extras a serem usadas por esta mesma camada no receptor. Neste ponto, a mensagem será denominada de segmento. Assim, cada camada insere suas informações de cabeçalho e repassa o pacote à camada subjacente, até chegar na camada física, que a depender do tipo de transmissão, transformará os \textit{bits} dos dados em sinais elétricos ou ondas eletromagnéticas, para os transmitir à rede, até o destino final. O termo pacote encontra-se de forma genérica para caracterizar um conjunto de dados, pois em cada camada possui um nome específico.

\begin{figure}[H]
	\label{fig:msg-tcpip}
	\includegraphics[width=0.5\textwidth]{../img/msg-tcpip.pdf}
	\centering
	\caption{Caminho de uma mensagem desde o remetente até o destinatário}
\end{figure}

De acordo com Tanenbaun \cite{TanenbaumBook}, a pilha \acrshort{TCP}/\acrshort{IP} surgiu da necessidade de conectar várias redes de maneira homogênea. Quando diversas universidades passaram a se conectar à \acrfull{ARPANET}, rede de pesquisa patrocinada pelo Departamento de Defesa dos Estados Unidos da América, usando diferentes protocolos, começaram a surgir problemas, que foram reparados com o advento do \acrshort{TCP}/\acrshort{IP}. Este protocolo permite a continuidade de comunicação entre dois \textit{hosts} ativos, mesmo se algumas linhas de transmissão intermediárias falharem. O \acrshort{TCP}/\acrshort{IP} possui capacidade de se flexibilizar em diferentes tipos de aplicações com requisitos distintos, pois sendo orientado a conexões, permite uma entrega confiável e sem erros de um conjunto de dados. De acordo com Kurose \cite{kurose}, o modelo divide-se em $5$ camadas: Aplicação, Transporte, Rede, Enlace e Física, conforme apresentado na Figura \ref{fig:tcp/ip}.

\begin{figure}[H]
	\includegraphics[width=0.15\textwidth]{../img/tcp-ip.pdf}
	\centering
	\caption{Camadas da pilha \acrshort{TCP}/\acrshort{IP}}
	\label{fig:tcp/ip}
\end{figure}

\subsection{Camada de Aplicação}
Posicionada no topo, logo acima da camada de transporte, constitui os protocolos de níveis mais elevados, responsáveis pelos diferentes tipos de comunicação de dados entre os usuários, permitindo que sistemas finais diferentes se comuniquem pela rede. Apresenta como principais protocolos o \acrfull{SMTP} que permite o envio de \textit{e-mails}, o \acrfull{HTTP} utilizado principalmente em sites, o \acrfull{FTP} usado para transferência de arquivos, e o \acrfull{DNS} responsável por traduzir endereços IP para nomes e vice-versa, possibilitando a identificação e localização de um determinado \textit{host} em um domínio.

\subsection{Camada de Transporte}
Localizada abaixo da camada de aplicação e acima da camada de rede mostra-se responsável por manter a comunicação entre dois computadores. Nesta camada, encontram-se dois principais protocolos, o \acrshort{TCP}, que apresenta como característica estabelecimento de conexão orientada, para a qual devem ser seguidos alguns passos definidos no protocolo a fim de ocorrer a interação entre os usuários, permitindo a entrega confiável sem erro dos segmentos, a recuperação de perdidos ou com dados corrompidos e a eliminação dos duplicados. Conta também com mecanismos para controlar o fluxo das mensagens, evitando que o receptor seja sobrecarregado no caso de o transmissor enviar mais segmentos do que o receptor consegue suportar.

Já, o \acrfull{UDP}, diferente do \acrshort{TCP}, por não possuir mecanismos que garantam ao receptor o recebimento de mensagens e por não haver necessidade de se estabelecer uma conexão entre os \textit{hosts}, definindo-se como não confiável. Embora existam meios de verificação da integridade dos segmentos, cabe ao programador implementar estes meios. Sua principal vantagem compreende a velocidade de processamento, tornando seu uso muito comum em serviços que necessitem de fluxo de dados em tempo real como jogos \textit{online} e \textit{streaming} de áudio e vídeo.

\subsection{Camada de Rede}
Posicionada logo abaixo da camada de transporte desempenha o papel de permitir o deslocamento de pacotes do transmissor ao receptor, mesmo em redes diferentes. Para tal, utiliza o \acrshort{IP}, um endereço virtual único, identificador de cada dispositivo na rede. Desse modo, quando um pacote chega à camada de Rede recebe endereços de \acrshort{IP} do remetente e do destinatário, para que durante seu caminho, cada nó da rede, pelo qual o pacote passe, saiba para onde encaminhá-lo. Aqui, faz-se necessária a distinção de duas funções desta camada evitando-se, pois, serem confundidas ou usadas como sinônimas: o repasse e o roteamento. O repasse ocorre no interior de um roteador, assim, quando um pacote chega ao seu enlace de entrada, o roteador examina um campo do cabeçalho do pacote e por meio da tabela de repasse escolhe o enlace de saída para o qual o pacote será conduzido. Já, o roteamento envolve todos os roteadores pelos quais o pacote passa, desde sua origem até o destino, ou seja, diz respeito ao caminho ou rota percorrida pelo pacote até chegar ao seu destinatário. Para estipular as rotas de um pacote, os roteadores utilizam os algoritmos de roteamento, cuja função residi em definir o melhor caminho a ser tomado por ele.  

\subsection{Camada de Enlace}
Logo abaixo da camada de rede, o enlace resulta na penúltima camada da pilha. Nela, roteadores e hospedeiros recebem o nome de nós e um canal de comunicação entre dois nós denomina-se enlace. Exerce a tarefa de enviar o datagrama (nome dado ao pacote na camada de enlace) do nó atual ao nó adjacente, por um único enlace de comunicação. Para tal, existem diferentes protocolos, como o \textit{Ethernet}, o \acrshort{IEEE} 802.11 - \textit{Wi-Fi}, o \acrfull{PPP}, entre outros. Vale salientar que um datagrama não necessariamente deve ser transportado pelo mesmo protocolo ao longo dos diferentes enlaces do caminho, posto que, a cada enlace, o protocolo pode ser diferente, o que não prejudica o movimento do datagrama para o proximo nó. Dentre os serviços oferecidos na camada de enlace figuram: enquadramento de dados, acesso ao enlace, entrega confiável, controle de fluxo, detecção de erros, correção de erros etc, que podem variar dependendo do protocolo utilizado. Geralmente, a camada de enlace encontra-se implementada em um adaptador de rede. Nesta camada, cada nó possui um adaptador de rede que por sua vez possui um endereço de camada de enlace identificador, conhecido como endereço \acrfull{MAC}.

\subsubsection{Endereços MAC}
Endereços físicos únicos (geralmente expressos em notação hexadecimal), identificadores do \textit{hardware} a ser utilizado para se conectar à rede. Formados por um conjunto de $6$ \textit{bytes} de comprimento, o que possibilita $2^{48}$ endereços diferentes. Como dito inicialmente, os endereços \acrshort{MAC} revelam-se únicos para cada adaptador e para garantir tal unicidade, o espaço físico desses endereços encontra-se controlado pelo instituto \acrshort{IEEE}. 

Neste cenário, uma empresa que deseja fabricar adaptadores de rede, deve entrar em contato com \acrshort{IEEE}, que fixará os primeiros $24$ \textit{bits} do endereço \acrshort{MAC}, e os associará a tal empresa, ficando os últimos $24$ \textit{bits} disponíveis para a empresa solicitante criar diferentes combinações para seus adaptadores. A Figura \ref{fig:endereco-mac} exemplifica esse processo. Como os nós possuem dois endereços diferentes, o \acrshort{IP} e o \acrshort{MAC}, torna-se necessário um mecanismo que realize a tradução entre eles, o que pode ser feito por meio do protocolo \acrfull{ARP}, maiores detalhes podem ser obtidos em \cite{kurose}.
\begin{figure}[H]
	\label{fig:endereco-mac}
	\includegraphics[width=0.4\textwidth]{../img/endereco-mac.pdf}
	\centering
	\caption{Endereço \acrshort{MAC} de acordo com a \acrshort{IEEE}}
\end{figure}
	
\subsubsection{Detecção de Erros}
A detecção de erros encontra-se implementada no \textit{hardware} e ocorre na camada de enlace, consistindo na identificação de quadros que possam chegar com erros ao receptor. Vale ressaltar que as técnicas de detecção de erro podem apresentar falhas, ou seja, mesmo com métodos de detecção, existe a possibilidade de um quadro que contenha erros não ser detectado pela camada de enlace e o mesmo ser repassado à camada de rede com erros. 

Quanto mais confiável o método de detecção, ou seja, quanto menor a chance de um falso negativo, maior o custo de processamento e maior a quantidade de \textit{bits} a ser transmitida junto aos dados. Essas técnicas de detecção se mostram necessárias, pois, de acordo com o modo de transmissão do dados, os \textit{bits} podem sofrer alguma alteração ao longo do caminho, devido aos campos magnéticos criados pela informação passando através dos cabos, ou a dispositivos faltosos. Canais de transmissão como as fibras óticas possuem pouca interferência, logo a chance de que os dados sejam corrompidos se torna menor; já, em meios de transmissão sem fio, a quantidade de ruído aumenta, tornando-os bastante suscetíveis a erros. Quando detecta-se um erro, geralmente, necessita-se o reenvio do pacote. Entre os principais métodos de detecção de erro, destacam-se: \acrfull{CRC}, verificação de paridade e o \textit{Checksum}.

\subsubsection{Cyclic Redundancy Check}
Técnica de detecção de erros disponível na camada de enlace, com funcionamento bastante simples. Primeiramente, remetente e destinatário devem  acordar com uma sequência de $r+1$ \textit{bits}, chamada de gerador e denotada por $G$, na qual o \textit{bit} mais à esquerda de $G$ deve ser $1$. O remetente que deseja enviar um dado de tamanho $D$ escolhe $r$ \textit{bits} adicionais $R$ e os anexa a $D$, de forma que ao $G$ dividir $D+R$ o resultado seja exato. Assim, quando o destinatário recebe $D+R$, torna-se possível saber se o dado encontra-se correto ou não. A Figura \ref{fig:crc} exemplifica o cálculo na verificação de uma mensagem recebida, sendo o resto da divisão igual a $0$, conclui-se que a mensagem recebida não possui erros.

\begin{figure}[H]
	\label{fig:crc}
	\includegraphics[width=0.4\textwidth]{../img/crc.pdf}
	\centering
	\caption{Exemplo do cálculo para o \acrshort{CRC}}
\end{figure}

\subsection{Camada Física}
Última camada da pilha \acrshort{TCP}/\acrshort{IP}, na qual se encontra a parte física, responsável por enviar ou receber fisicamente os dados da rede. Há basicamente três tipos de transmissão: cabeada, sem fio e por satélite. Cada um desses modos de transmissão possui diferentes propriedades, capazes de interferir diretamente na performance e estrutura da rede utilizada. Sua unidade básica constitui-se em \textit{bits}, que podem ser transmitidos por meio de sinais elétricos (no caso de redes cabeadas) ou de ondas eletromagnéticas (em redes sem fio). 

\section{Tecnologias para Comunicação Sem Fio} \label{Tec_comm}
Redes de computadores crescem a cada dia, devido a esse crescimento existem algumas divisões que limitam o tamanho da rede \cite{TanenbaumBook}, como \acrfull{PAN}, \acrfull{LAN}, \acrfull{MAN} e \acrfull{WAN}. Cada uma dessas redes possui uma abrangência diferente, sendo \acrshort{WAN} a maior entre elas, utilizada para transmissão de dados em longa distância, entre países e para outras \acrshort{WAN}, \acrshort{LAN} ou \acrshort{MAN}. Redes \acrshort{LAN}, por outro lado, conectam ambientes menores, como escolas, empresas, residências entre outros. A Figura \ref{fig:range-net} ilustra dispositivos e abrangência das redes citadas.

\begin{figure}[H]
	\centering
	\label{fig:range-net}
	\includegraphics[width=0.3\textwidth]{./img/range-net.pdf}
	\caption{Abrangência das diferentes redes}
\end{figure}

As tecnologias mais comuns para este tipo de rede consistem em \textit{Ethernet} e \textit{Wi-Fi}. Por fim, uma \acrshort{PAN} ou \acrshort{WPAN}, representam redes pequenas, compostas por celulares, computadores, \textit{tablets}, \textit{beacons} comumente baseadas no padrão \acrshort{IEEE} 802.15 \cite{IEEE802.15}. O objetivo dessas redes se resume em facilitar operações entre dispositivos de casa ou empresas e sistemas através de comunicação de curta distância, em geral até $10$ metros de distância, não necessariamente conectado à Internet e operam de mesma forma a outras redes através das camadas de rede. Nas redes \acrshort{WPAN} congregam diversas tecnologias, entre elas a \acrfull{IRDA}, presente em controles de televisão; o padrão \acrshort{IEEE} 802.15.4 encontrado em chips proprietários (\textit{Zigbee}) \cite{IEEE802.15.4}; o padrão \acrshort{IEEE} 802.15.1 - \textit{Bluetooth} \cite{IEEE802.15.1}, em controles de mídia para computadores, \textit{mouses}, celulares e também \textit{beacons} entre outros dispositivos. Portanto, encontra-se nas redes \acrshort{WPAN} a utilização da \acrshort{IoT} e da \textit{Physical Web}.

O foco principal da pesquisa recai no \textit{Bluetooth}, devido, não somente, ao objetivo de prover um aplicativo \textit{Android} para localização e posicionamento em ambientes internos, como também pela raridade de celulares usufruírem da tecnologia contida em \textit{chips} \textit{Zigbee}, \textit{Z-Wave} ou outras presentes na \acrshort{WPAN}. Entretanto, dispositivos modernos, em geral, incorporam a tecnologia \textit{Bluetooth} (versão $4.0$ ou superior), o que oferece suporte para \acrshort{BLE}, possibilitando um baixo consumo de energia, fator indispensável para dispositivos móveis.

\subsection{\acrshort{IEEE} 802.15}
Define-se por uma família do \acrfull{IEEE} para especificar padrões de comunicação sem fio para as redes \acrshort{WPAN}, especificamente as camadas física e de enlace. Dentre os padrões mais conhecidos destacam-se o \acrshort{IEEE} 802.15.1 - \textit{Bluetooth}, o \acrshort{IEEE} 802.15.3, para \acrshort{WPAN} que necessitam de taxas de transmissão altas, um máximo de $5{,}3$GBits/s, e o \acrshort{IEEE} 802.15.4 para taxas de transmissões pequenas.

\subsection{\acrshort{IEEE} 802.15.1 - \textit{Bluetooth}} \label{Bluetooth}

\textit{Bluetooth} refere-se a uma tecnologia de comunicação sem fio que possibilita dispositivos se comunicarem por pequenas distâncias e opera sobre a banda não licenciada de $2{,}4$ GHz. A tecnologia possui um grupo regulador conhecido por \acrfull{BSIG}, padronizado pelo \acrshort{IEEE} no \acrshort{IEEE} 802.15.1 \cite{bsig}.

\begin{figure}[H]
	\label{fig:slavemaster}
	\includegraphics[width=0.5\textwidth]{../img/slave-master.pdf}
	\centering
	\caption{Dispositivo mestre estabelecendo conexões com os escravos}
\end{figure}

O protocolo \textit{Bluetooth} utiliza $79$ canais para transmissão de dados, começando na frequência $2402$ e terminando em $2408$ MHz. Para diminuir a interferência com outros sinais de rádio se utiliza uma técnica conhecida por \textit{frequency hopping} \cite{BluetoothBook}, para o dado ser transmitido em canais diferentes periodicamente. Neste cenário, torna-se necessário estabelecer uma conexão entre os dispositivos.

Um dispositivo \textit{Bluetooth} possui capacidade para conversar com até $7$ outros dispositivos, simultaneamente, por meio do método chamado de ``mestre-escravo'', de modo que quem inicia a conexão se torna o mestre da rede, demonstrado na Figura \ref{fig:slavemaster}. Existem, também, dispositivos para os quais a conexão não se torna necessária, conhecidos como \textit{broadcasters}. Neles a troca de informações se faz em mão única, ou seja, apenas enviam informação, não podendo receber, como por exemplo, relógios inteligentes para monitoramento cardíaco, ilustrado na Figura \ref{fig:smartwatch}. Com a disseminação da \acrshort{IoT} e da \acrshort{PW}, houve a necessidade de se desenvolver dispositivos \textit{Bluetooth} que consumam menos energia, surgindo assim a tecnologia \acrshort{BLE}.

\begin{figure}[H]
	\label{fig:smartwatch}
	\includegraphics[width=0.3\textwidth]{../img/smartwatch.jpg}
	\centering
	\caption{Relógio monitor de batimentos cardíacos}
\end{figure}


\subsubsection{\textbf{\textit{Bluetooth Low-Energy}}}

\acrshort{BLE} ou \textit{Bluetooth Smart} consiste em uma derivação do \textit{Bluetooth Core} $4.0$ e objetiva reduzir a energia gasta durante o seu funcionamento. Isto ocorre devido ao modo de divulgação de pacotes, otimizado de tal maneira que o dispositivo permanece em estado dormente na maior parte de seu tempo de atividade, contribuindo também para o baixo consumo de energia. Sua potência dispõe de valor máximo em $0$ dBm para um alcance teórico de $10$m de distância, seu uso comum apresenta-se entre $-40$ a $-70$ dBm. O \acrshort{BLE} resulta diferente do \textit{Bluetooth} usual devido a uma técnica de modulação especial utilizada. Este tipo de \textit{Bluetooth} possui capacidade de operar em quatro modos: periférico, central, \textit{broadcast} e observador.

\begin{enumerate}
	\item \textbf{Periférico} - Anunciante, opera em modo escravo e pode receber conexões.
	\item \textbf{Central} - Varredor, pode iniciar conexões e trabalha como mestre em uma ou mais conexões.
	\item \textbf{\textit{Broadcast}} - Anunciante, não conectável.
	\item \textbf{Observador} - Varredor, não conectável.
\end{enumerate}

Para \textit{beacons}, o primeiro e o terceiro modo, que se referem a anunciantes, constituem os principais. Ambos enviam o mesmo tipo de pacote, entretanto, mudando-se uma indicação eles se tornam conectáveis ou não. O modo conectável indica o dispositivo \textit{Bluetooth Smart}, podendo receber conexões que mudam o estado de seus dados de maneira simples. Muitos \textit{beacons} possuem essa tecnologia para que seja possível modificar livremente o anúncio em transmissão. No caso do \textit{Smart}, isso pode ser feito por conexão de aplicativos e/ou \acrfull{API}/\acrfull{SDK} da empresa fornecedora do \textit{beacon}. Já para os não conectáveis se faz necessária a utilização de \acrfull{USB}, \acrfull{SPI} ou alguma conexão cabeada semelhante. Na Tabela \ref{tab:PDUt} encontra-se o código referente a cada tipo de funcionamento para o modo anunciante e varredor. Este código aparece associado a parte de \acrfull{PDU} evidenciado na Figura \ref{fig:BLE-PDU}. Neste pacote, há uma divisão de cabeçalho e de dados (\textit{payload}), na primeira parte encontram-se os dados para identificação de modo de operação e na segunda, informações referentes ao anúncio.

\begin{table}[H]
	\caption{Tipos de PDU}
	\label{tab:PDUt}
	\centering
	\begin{tabular}{c|c|c}
		\hline
		PDU  & Nome Pacote       & Descrição      \\ \hline \hline
		0000 & ADV\_IND          & Conectável     \\ 
		0001 & ADV\_DIRECT\_IND   & Conectável     \\ 
		0010 & ADV\_NONCONN\_IND & Não conectável \\ 
		0011 & SCAN\_REQ          & Varredor       \\ 
		0100 & SCAN\_RESP         & Resposta       \\ 
		0101 & CONNECT\_REQ       & Resposta       \\ 
		0110 & ADV\_SCAN\_IND    & Não conectável \\ \hline \hline
		
	\end{tabular}
\end{table}


\begin{figure}[H]
	\label{fig:BLE-PDU}
	%http://www.argenox.com/bluetooth-low-energy-ble-v4-0-development/library/a-ble-advertising-primer/
	\includegraphics[width=0.5\textwidth]{./img/ble_pdu.pdf}
	\centering
	\caption{Modelo de pacotes \textit{Bluetooth}}
\end{figure}

O \acrshort{BLE} utiliza a mesma frequência de diversos outros protocolos de comunicação sem fio, banda $2{,}4$ GHz \acrfull{ISM}, como por exemplo \textit{Wi-Fi} e telefones sem fio, e ainda eletrodomésticos como o micro-ondas, permitindo que ocorra interferência entre os sinais. Diferentemente do \textit{Bluetooth} convencional, o \acrshort{BLE} utiliza $40$ canais de $2$MHz cada, sendo três para anúncios, como apresentado na Figura \ref{fig:BLE-chan}.

\begin{figure}[H]
	\label{fig:BLE-chan}
	%http://www.argenox.com/bluetooth-low-energy-ble-v4-0-development/library/a-ble-advertising-primer/
	\includegraphics[width=0.5\textwidth]{./img/ble-chan.pdf}
	\centering
	\caption{Canais para anunciação BLE}
\end{figure}

Esses canais foram escolhidos para gerar menos conflito com os mais usados pelo sinal \textit{Wi-Fi}, como pode ser visto na Figura \ref{fig:BLE-spectrum}. Entretanto, como \textit{Wi-Fi} possui maior potência em relação a dispositivos \acrshort{BLE}, o sinal pode ser distorcido, provocando erro nos dados, caso os sinais sejam transmitidos muito próximos uns dos outros. Vale lembrar que, por mais estrategicamente posicionados para não interferir com os canais \textit{Wi-Fi} mais usados, a possibilidade de interferência continuará existindo \cite{TIReport}. 

\begin{figure}[H]
	\label{fig:BLE-spectrum}
	%http://www.argenox.com/bluetooth-low-energy-ble-v4-0-development/library/a-ble-advertising-primer/
	\includegraphics[width=0.5\textwidth]{./img/ble-spectrum.pdf}
	\centering
	\caption{Espectro \acrshort{BLE} X \textit{Wi-Fi}}
\end{figure}

\subsection{\acrshort{IEEE} 802.15.4} \label{Zigbee}
De acordo com Gutierrez \cite{2005:802.15.4}, o protocolo \acrshort{IEEE} 802.15.4 se define por uma \acrshort{WPAN} de baixo custo, em geral menor que uma rede \textit{Bluetooth}, e portanto esse protocolo diminui o alcance do sinal e velocidade de transmissão de dados, apresentando, geralmente, taxas abaixo de $0{,}25$ Mb/s e apenas $16$ canais operando na faixa \acrshort{ISM} na frequência de $2{,}4$ GHz. Para trabalhar de maneira conjunta ao \textit{Wi-Fi}, padrão \acrshort{IEEE} 802.11, necessita-se que a frequência entre os canais ocorre a cada $5$MHz em relação aos $25$MHz do \textit{Wi-Fi}, como mostra a Figura \ref{fig:zbchan}, isso permite diminuir a interferência entre os sinais. Para uma interferência ainda mais baixa, Liang \textit{et al}. \cite{2010:interference} expõem, entre outras técnicas, a possibilidade de identificar os pacotes para que um transmissor \textit{Wi-Fi} aguarde, a fim de deixar o \acrshort{IEEE} 802.15.4 avançar durante um certo período de tempo.

\begin{figure}[H]
	\includegraphics[width=0.5\textwidth]{../img/zigxwifichan.pdf}
	\centering
	\caption{Coexistência \acrshort{IEEE} 802.15.4 e \acrshort{IEEE} 802.11}
	\label{fig:zbchan}
\end{figure}

Dentre as tecnologias aqui apresentadas para o \acrshort{IEEE} 802.15.4, destaca-se a \textit{Zigbee}, desenvolvida para uso residencial. Essa tecnologia não se encontra completamente definida dentro do padrão da \acrshort{IEEE}, pois apenas utiliza as camadas \acrshort{MAC} e as camadas físicas, como mostra a Figura \ref{fig:zbarc}.

\begin{figure}[H]
	\includegraphics[width=0.5\textwidth]{../img/zigbeearch.pdf}
	\centering
	\caption{Arquitetura \textit{Zigbee} (adaptado de Rivero \cite{ZBLE})}
	\label{fig:zbarc}
\end{figure}

\textit{Zigbee} constrói sobre as camadas seus próprios métodos, a fim de garantir melhor custo/benefício em relação ao consumo de energia e a vazão. As camadas \acrshort{MAC} e física mantêm o padrão \acrshort{IEEE} 802.15.4, porém, a camada de rede e aplicação possuem interfaces e \textit{softwares} especificados de maneira proprietária pela empresa \textit{Zigbee Alliance}, criadora do protocolo. Assim, de acordo com a empresa, o \textit{Zigbee} possui maior segurança, confiabilidade e poder de transmissão quando comparado ao padrão \acrshort{IEEE} 802.15.4. Vale lembrar, que tais vantagens trazem consigo um custo elevado, em relação a concorrentes, devido a natureza privada de suas tecnologias.


\section{\textit{Physical Web}} \label{Physical Web}

Diversas empresas começaram a fabricar \textit{hardware} com o suporte ao \acrshort{BLE}, porém, cada dispositivo possuía sua própria especificação, dificultando para o desenvolvedor criar e dar manutenção às suas aplicações. Com a finalidade de promover a \acrshort{PW} a \textit{Google} projetou o Eddystone, modelo de padronização da informações contidas no campo \textit{payload} do protocolo \acrshort{BLE}, rivalizando o modelo iBeacon da \textit{Apple}.

A \acrshort{PW} fundamenta-se em um conceito que tem como princípio a proximidade, relacionando objetos físicos com interações, proporcionando conteúdos com maior interesse ao usuário, baseado em sua localidade. As aplicações possíveis na \acrshort{PW} mostram-se inúmeras e podem facilitar o dia-a-dia de diversas maneiras, por exemplo: um ponto de ônibus pode fornecer informações a respeito das linhas que por ali circulam; a coleira de um cachorro pode ser usada para localizá-lo ou informar às pessoas próximas a respeito de seu dono; um cliente em uma loja pode receber ofertas sobre produtos de seu interesse.

%\textbf{ADICIONAR DEFINIÇÃO DE PW AQUI, falar que com o surgimento da PW apareceu a necessidade do encapsulamento e depois as empresas começaram a fabricar os beacons}

\subsection{\textit{Beacons}}
Constituem dispositivos \acrshort{BLE}, e assim aproveitam do gasto de energia minimizado, podendo utilizar baterias menores, como moeda ou pilhas, alguns modelos podem utilizar conexão USB ou uma tomada de energia para seu funcionamento. Além disso, possuem um tamanho pequeno, ideais para serem carregados nos bolsos ou colocados em mochilas e paredes de um ambiente, sem qualquer incomodo aos usuários. Alguns modelos se encontram na Figura \ref{fig:beacons_modelos}.
\begin{figure}[H]
	\includegraphics[width=0.7\textwidth]{../img/beacons.png}
	\centering
	\caption{Modelos de dispositivos \acrshort{BLE}}
	\label{fig:beacons_modelos}
\end{figure}
\textit{Beacons} desempenham a função de enviar, periodicamente, pequenos pacotes de dados, padronizados em protocolos de encapsulamento de informações, utilizados para formatar o campo de \textit{payload} do \acrshort{BLE}. Possui, portanto, $37$ \textit{bytes} livres para envio de informação. Mesmo com o menor gasto de energia os \textit{beacons} se mantém em estado de espera (sonolento) pela maior parte do tempo, reativando (acordando) em intervalos predefinidos para envio de informação.

\subsection{Protocolos de Encapsulamento de Informações} \label{Encapsulamento}
Os protocolos de encapsulamento de informações objetivam segmentar $32$ \textit{bytes} dos $37$ de dados, encontrados no \textit{payload}, os últimos $5$ \textit{bytes} ficam reservados para uso futuro, em pequenos campos que contêm informações úteis para o cliente, como potência, frequência do sinal, nome do dispositivo e identificação. Os protocolos mais populares foram desenvolvidos por duas grandes empresas, \textit{Apple} com o iBeacon e \textit{Google} com Eddystone, estes serão detalhados abaixo.

\subsubsection{iBeacon} \label{Encap_ibeacon}

O iBeacon consiste em um serviço de notificação sem fio, criado pela \textit{Apple}, em $2013$, com a finalidade de transmitir páginas \textit{Web}, via \textit{smartphones}, entre dois usuários. Considerado o primeiro modelo de formatação de dados \acrshort{BLE} bem aceito.

Este sistema se encontra composto por campos, sendo que o campo \acrfull{UUID} especifica o aplicativo que usará o \textit{beacon}. Todo dispositivo com mesma identificação se relaciona com um único \textit{software}. Os campos \textit{Major} e \textit{Minor} ocupam-se em delimitar sub-regiões, onde \textit{Major} demarca o subgrupo e \textit{Minor} um \textit{beacon} individual. O campo $TX_{Power}$ representa a potência do dispositivo, calibrado pela da força do sinal recebido a $1$m de distância do receptor \cite{ibeacon:2014}. A Figura \ref{fig:ib_spec} ilustra o formato do protocolo.

\begin{figure}[H]
	\label{fig:ib_spec}
	\centering
	\includegraphics[width=0.5\textwidth]{./img/ibeacon_spec.pdf}
	\caption{Especificação da moldura do iBeacon}
\end{figure}

Este protocolo apresenta algumas limitações, como a taxa de intervalo entre transmissões fixa em $100$ms, o que provoca um gasto de energia maior que o necessário, fazendo com que a carga da bateria termine rapidamente. Além disso, devido à \acrshort{UUID} estrita para cada aplicação, há a obrigatoriedade do uso de uma aplicação diferente para cada empresa ou loja que deseja implementar um sistema com iBeacon em seus projetos. Embora exista a possibilidade de portabilidade para \textit{Android}, em geral, restringe-se a produtos \textit{Apple}.

\subsubsection{Eddystone} \label{Encap_Eddy}

Desenvolvido pela \textit{Google} e disponibilizado ao público em $2015$, o Eddystone consiste em uma especificação de protocolo para definir o formato de mensagens utilizadas em \textit{beacons}. Essas mensagens encontram-se re-encapsuladas em cima do protocolo de comunicação \acrshort{BLE}. Entre os objetivos do Eddystone registram: o bom funcionamento tanto em \textit{Android} quanto em \textit{IOS}, a compatibilidade com a especificação \textit{Bluetooth Core} e a fácil implementação em diversos dispositivos \acrshort{BLE}. O protocolo possui diferentes molduras para envio de informação, entre elas destacam-se como mais importantes: \acrfull{UID}, \acrshort{URL}, \acrfull{TLM} presentes na Tabela \ref{tab:frames} e as especificações das molduras podem ser vistas na Figura \ref{fig:frametypes}.
\begin{table}[H]
	\label{tab:frames}
	\centering
	\caption{Indicação para tipos de moldura}
	\begin{tabular}{c|c|c}
		\hline
		\textbf{Frame Type} & \textbf{High-Order 4 \textit{bits}} & \textbf{Byte Value} \\ \hline \hline
		UID                 & 0000                       & 0x00                \\ 
		URL                 & 0001                       & 0x10                \\ 
		TLM                 & 0010                       & 0x20                \\ 
		EID                 & 0011                       & 0x30                \\ 
		RESERVED            & 0100                       & 0x40                \\ \hline \hline
	\end{tabular}
\end{table}

\begin{itemize}
	\item EddyStone-UID \\
	Um tipo de moldura sem criptografia que transmite de maneira \textit{broadcast} uma carga útil de $16$ \textit{bytes}. Dentro destes \textit{bytes} há uma divisão, os primeiros $10$ limitam o \textit{namespace} e os últimos $6$, a instância dos \textit{beacons}.
	
	O \textit{namespace} serve para os reunir em um mesmo grupo virtual, desta forma se sabe qual \textit{beacon} pertence a qual aplicação, empresa ou indivíduo. Por outro lado, o número de instância serve para identificar um \textit{beacon} específico dentro destes grupos.
	
	%Por recomendação da \textit{Google}, pode-se utilizar dois métodos para gerar os \textit{namespaces} de maneira única: \textit{Truncated Hash} sobre o nome de domínio e \textit{Elided \acrshort{UUID}}.
	
	Além desse formato, o Eddystone conta também com \textit{Ephemeral Identification} que possui as mesmas propriedades do \acrshort{UID}, porém com criptografia associada, de modo que apenas pessoas autorizadas possam acessar a informação.
	\item EddyStone-URL \\
	A moldura \acrshort{URL} foi desenvolvida para ser a coluna da \acrshort{PW}, tornando conteúdos \textit{web} fortemente relacionados com o indivíduo e seu entorno.
	Nessa moldura, a divisão de \textit{bytes} ocorre de modo que os últimos $17$ servem ao propósito principal: codificar uma \acrshort{URL} para ser transmitida pelo \textit{beacon}. Os primeiros \textit{bytes} resumem-se organizados como no \acrshort{UID}, mas utiliza o terceiro \textit{byte} como prefixo da \acrshort{URL}, como exemplificado na Tabela \ref{tab:URLprefix}.
	\begin{table}[H]
		\label{tab:URLprefix}
		\centering
		\caption{Prefixo para terceiro \textit{byte} Eddystone-URL}
		\begin{tabular}{c|c|c}
			\hline
			\textbf{Decimal} & \textbf{Hexa} & \textbf{Prefix} \\ \hline \hline
			0                & 0x00          & http://www.     \\ 
			1                & 0x01          & https://www.    \\ 
			2                & 0x02          & http://         \\ 
			3                & 0x03          & https://        \\ \hline \hline
		\end{tabular}
	\end{table}
	\item EddyStone-TLM \\
	Esta última moldura serve para enviar informações referentes ao \textit{beacon} propriamente dito, tais como voltagem ou temperatura.
\end{itemize}
\begin{figure}[H]
	\label{fig:frametypes}
	\centering
	\includegraphics[width=0.4\textwidth]{./img/frametypes_eddy.pdf}
	\caption{Moldura dos pacotes UID,URL e TLM do Eddystone}
\end{figure}


\section{Discussão}

Aborda-se a fundamentação teórica para situar o leitor acerca dos assuntos necessários para compreensão do estudo. Primeiramente explica-se a estrutura da pilha \acrshort{TCP}/\acrshort{IP}. Segundamente, as tecnologias para comunicação sem fio em redes \acrshort{WPAN} e, por fim, defini-se a \textit{Physical Web} e os dispositivos e protocolos utilizados em seu funcionamento. No Capítulo \ref{cap3} será explicado os mecanismos para localização de dispositivos \acrshort{BLE}.