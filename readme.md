#Resumo

A possibilidade de pequenos dispositivos informando um serviço constitui um dos principais aspectos previstos pela Physical Web (PW). A PW consiste em uma tecnologia
que visa fornecer interações contínuas com objetos físicos e suas respectivas localizações.
Os dispositivos da PW utilizam o protocolo Bluetooth Low Energy (BLE) para fornecer
conteúdo e informações aos usuários em localizações bastante específicas. Contudo, para
atingir o seu objetivo, esses dispositivos devem ser capazes de fornecer informações de
localização a um nível aceitável. Este estudo investiga a precisão de informações de localização fornecida por um dispositivo BLE. Para isso, implementou-se uma test-bed por
meio do Google Eddystone com beacons BLE e avaliou-se sua precisão de localização usando as técnicas de estimativa de localização baseadas no Indicador de Intensidade do
Sinal Recebido (do Inglês, Received Signal Strength Indicator) (RSSI). Os resultados experimentais mostraram que os beacons BLE utilizados foram capazes de fornecer uma
estimativa de localização aceitável, fornecendo acurácia média de 0,20m, para distâncias
de até 5 metros.

**Palavras-chave:** Bluetooth Low Energy, Posicionamento, Ambientes internos, Filtro Kalman,
Estimativa de distância, Physical Web, Trilateração, Beacons

![tela food truck](./CIC/img/app_new_scan_small.jpg "Tela para encontrar produtos próximos a localização do Beacon")