import math

x = float('nan');
cov = 0;
Q = [1,1.2,1.5,2,3,4];
R = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1];

def kalman(z,q,r):
	global x;
	global cov;
	if(math.isnan(x)):
		x = z;
		cov = q;
	else:
		predX = x;
		predCov = cov + r;

		k = predCov * 1/(predCov+q);
	
		x = predX + k *(z-predX);
		cov = predCov - (k * predCov);
	return x;

def script():
	with open("saida.txt", "w") as fileS:
		for q in Q:
			for r in R:
				fileS.write("Q = %.1f R = %.1f\n" % (q, r));
				cov = 0;
				x = float('nan');
				for rssi in RSSI:
					fileS.write("%d;%.2f\n" % (rssi,  kalman(rssi, q, r)));

if __name__ == "__main__":
	RSSI = [];
	with open("RSSI_1m.txt") as file:
		lines = file.readlines();
		for l in lines:
			RSSI.append(int(l));

	for rssi in RSSI:
		print("%d %.2f" % (rssi,  kalman(rssi, 1.5, 0.7)));

	