second2last = 0
last = 0
def media_erro(vetor):
	if(vetor==[]):
		return 0
	else:
		soma = 0
		for i in vetor:
			soma+=i
		media = soma/len(vetor)
		#print(media)
		return media
def filtro(RSSI,RSSI_filtrado,An,Bn):
		global second2last
		global last
		erro = media_erro(RSSI_filtrado)
		if(len(RSSI_filtrado) > 1):
			result = (1 - An) * second2last + An * (erro + (RSSI - erro) * Bn)
		else:
			result = RSSI
		second2last = last
		last = result
		string = "%.2f" % last
		print(string.replace(".",","))
		RSSI_filtrado.append(result)

if __name__ == "__main__":
	RSSI_filtrado = []
	RSSI = []
	Ab = 0.7
	Bb = 0.05
	Wn = 0.9
	An = 0.9
	Bn = 0.1
	with open("RSSI_1m.txt") as file:
		lines = file.readlines()
		for l in lines:
			RSSI.append(int(l))
	for i in RSSI:
		if((An > Ab) and (Bn > Bb)):
			An *= Wn
			Bn *= Wn
		filtro(i,RSSI_filtrado,An,Bn)
	#for i in RSSI_filtrado:
		#print(str(i))
