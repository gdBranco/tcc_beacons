package unb.beacon.beacon_project.Utilidades;

/**
 * Created by samuelpala on 1/4/17.
 */

public class Produto {

    public String nome;
    public Double preco;
    public String descricao;
    public String imagem;
    public String id;
    public String logo;
    public Double dist;

    public Produto (String _nome, Double _preco, String _descricao, String _imagem, String _id, String _logo){
        this.nome = _nome;
        this.preco = _preco;
        this.descricao = _descricao;
        this.imagem = _imagem;
        this.id = _id;
        this.logo = _logo;
        for(Beacon b : AppData.getInstance().lista_beacons)
        {
            if(b.id.endsWith(this.id))
            {
                this.dist = b.getDistance(Utilidades.STD_DIST);
                break;
            }
        }
    }


}
