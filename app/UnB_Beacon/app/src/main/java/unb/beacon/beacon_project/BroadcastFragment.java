package unb.beacon.beacon_project;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import unb.beacon.beacon_project.Utilidades.ScannerManager;
import unb.beacon.beacon_project.Utilidades.SharedPreferencesManager;
import unb.beacon.beacon_project.Utilidades.Utilidades;


/**
 * A simple {@link Fragment} subclass.
 */
public class BroadcastFragment extends Fragment {
    private final static String TAG = BroadcastFragment.class.getSimpleName();
    private BluetoothLeAdvertiser adv;
    private AdvertiseCallback advertiseCallback;
    int txPowerLevel;
    int adMode;
    TextView text_hex_name;
    TextView text_hex_id;

    public BroadcastFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_broadcast, container, false);
        TextView text_namespace = (TextView) view.findViewById(R.id.text_namespace);
        text_namespace.setText("Namespace");
        TextView text_id = (TextView) view.findViewById(R.id.text_id);
        text_id.setText("Instance");
        text_hex_name = (TextView) view.findViewById(R.id.text_hex_namespace);
        text_hex_id = (TextView) view.findViewById(R.id.text_hex_id);
        Button rnd_id = (Button) view.findViewById(R.id.RND_ID);
        Button rnd_name = (Button) view.findViewById(R.id.RND_namespace);
        Spinner power = (Spinner) view.findViewById(R.id.spinner_power);
        Spinner latency = (Spinner) view.findViewById(R.id.spinner_latency);
        ToggleButton start = (ToggleButton) view.findViewById(R.id.toggle_broad);
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        adv = adapter.getBluetoothLeAdvertiser();
        advertiseCallback = createAdvertiseCallback();


        txPowerLevel = SharedPreferencesManager.getInstance().getInt(Utilidades.P_TXPOWER);
        adMode = SharedPreferencesManager.getInstance().getInt(Utilidades.P_TXMODE);
        text_hex_name.setText(SharedPreferencesManager.getInstance().getString(Utilidades.P_NAMESPACE));
        text_hex_id.setText(SharedPreferencesManager.getInstance().getString(Utilidades.P_INSTANCE));
        if(rnd_name!=null) {
            rnd_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    text_hex_name.setText(Utilidades.randomHexString(10));
                    SharedPreferencesManager.getInstance().putString(Utilidades.P_NAMESPACE,text_hex_name.getText().toString());
                }
            });
        }
        if(rnd_id!=null) {
            rnd_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    text_hex_id.setText(Utilidades.randomHexString(6));
                    SharedPreferencesManager.getInstance().putString(Utilidades.P_INSTANCE,text_hex_id.getText().toString());
                }
            });
        }
        ArrayAdapter<CharSequence> txpowerAdapter = ArrayAdapter.createFromResource(getContext(),R.array.tx_power_array,android.R.layout.simple_spinner_dropdown_item);
        txpowerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        power.setAdapter(txpowerAdapter);
        power.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = (String) parent.getItemAtPosition(position);
                txPowerLevel = Utilidades.getPowerLevel(getContext(),selected);
                SharedPreferencesManager.getInstance().putInt(Utilidades.P_TXPOWER, txPowerLevel);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // NOP
            }
        });
        power.setSelection(txPowerLevel);
        ArrayAdapter<CharSequence> txmodeAdapter = ArrayAdapter.createFromResource(getContext(),R.array.tx_mode_array,android.R.layout.simple_spinner_dropdown_item);
        txmodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        latency.setAdapter(txmodeAdapter);
        latency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = (String) parent.getItemAtPosition(position);
                adMode = Utilidades.getADMode(getContext(),selected);
                SharedPreferencesManager.getInstance().putInt(Utilidades.P_TXMODE, adMode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // NOP
            }
        });
        latency.setSelection(adMode);
        start.setTextOff("Parado");
        start.setTextOn("Broadcasting");
        start.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    startAdvertising();
                } else {
                    stopAdvertising();
                }
            }
        });
        return view;
    }
    private void startAdvertising()
    {
        showToast(String.format("NAMESPACE: %s\nINSTANCE: %s\nPOWERLEVEL: %s\nADMODE: %s",
                text_hex_name.toString(),
                text_hex_id.toString(),
                Utilidades.getPowerLevel(getContext(),txPowerLevel),
                Utilidades.getADMode(getContext(),adMode)));
        AdvertiseSettings advertiseSettings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(adMode)
                .setTxPowerLevel(txPowerLevel)
                .setConnectable(true)
                .build();

        byte[] serviceData = null;
        try {
            serviceData = buildServiceData();
        } catch (IOException e) {
            Log.d(TAG,e.toString());
        }

        AdvertiseData advertiseData = new AdvertiseData.Builder()
                .addServiceData(Utilidades.SERVICE_UUID, serviceData)
                .addServiceUuid(Utilidades.SERVICE_UUID)
                .setIncludeDeviceName(false)
                .setIncludeTxPowerLevel(false)
                .build();
        adv.startAdvertising(advertiseSettings, advertiseData, advertiseCallback);
    }

    private void stopAdvertising() {
        adv.stopAdvertising(advertiseCallback);
    }

    private byte[] buildServiceData() throws IOException
    {
        byte btxpower = Utilidades.txPowerLevelToByteValue(txPowerLevel);
        byte[] bnamespace = Utilidades.toByteArray(SharedPreferencesManager.getInstance().getString(Utilidades.P_NAMESPACE));
        byte[] binstance = Utilidades.toByteArray(SharedPreferencesManager.getInstance().getString(Utilidades.P_INSTANCE));
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        os.write(new byte[]{Utilidades.FRAME_TYPE_UID,btxpower}); // 2byte
        os.write(bnamespace); // 10 bytes
        os.write(binstance); // 6 bytes
        return os.toByteArray();
    }

    private AdvertiseCallback createAdvertiseCallback() {
        return new AdvertiseCallback() {
            @Override
            public void onStartFailure(int errorCode) {
                switch (errorCode) {
                    case ADVERTISE_FAILED_DATA_TOO_LARGE:
                        showToast("ADVERTISE_FAILED_DATA_TOO_LARGE");
                        break;
                    case ADVERTISE_FAILED_TOO_MANY_ADVERTISERS:
                        showToast("ADVERTISE_FAILED_TOO_MANY_ADVERTISERS");
                        break;
                    case ADVERTISE_FAILED_ALREADY_STARTED:
                        showToast("ADVERTISE_FAILED_ALREADY_STARTED");
                        break;
                    case ADVERTISE_FAILED_INTERNAL_ERROR:
                        showToast("ADVERTISE_FAILED_INTERNAL_ERROR");
                        break;
                    case ADVERTISE_FAILED_FEATURE_UNSUPPORTED:
                        showToast("ADVERTISE_FAILED_FEATURE_UNSUPPORTED");
                        break;
                    default:
                        showToast("startAdvertising failed with unknown error " + errorCode);
                        break;
                }
            }
        };
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

}
