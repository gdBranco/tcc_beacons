package unb.beacon.beacon_project.Utilidades;

import android.app.ActivityManager;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

import unb.beacon.beacon_project.R;

/**
 * Created by dbranco on 03/06/2016.
 */
public class Beacon {

    private static final String TAG = Beacon.class.getSimpleName();
    public String name;
    public String id;
    public int power;
    public double m_rssi_movel;
    public double m_rssi_kalman;
    public double m_rssi_doublefilter;
    public int r_rssi;
    public double m_distance_movel;
    public double m_distance_kalman;
    public double m_distance_doublefilter;
    public double[] pos = {0.0,0.0};
    public double raio = 0.0;
    public long last_ts;
    public double n = 2;
    public double rssi_Cn = 0;
    public KalmanFilter kalman = new KalmanFilter(0.,1.5);
    public MediaMovel media = new MediaMovel(Utilidades.MAXCALIBRAGEM);
    public DoubleFilter dfilter = new DoubleFilter(0.9,0.1);
    public double A = -57;
    public ArrayList<Produto> produtos;
    DatabaseReference beaconref;

    public double getDistance(int type)
    {
        switch(type)
        {
            case Utilidades.KALMAN:
                return getDistanceKalman();
            case Utilidades.MOVEL:
                return getDistanceMovel();
            case Utilidades.DFILTER:
                return getDistanceDFilter();
        }
        return 0;
    }

    public double getRSSI(int type)
    {
        switch(type)
        {
            case Utilidades.KALMAN:
                return getMediaKalman();
            case Utilidades.MOVEL:
                return getMediaMovel();
            case Utilidades.DFILTER:
                return getMediaDFilter();
        }
        return 0;
    }

    private double getDistanceMovel(){return this.m_distance_movel;}

    private double getDistanceKalman(){return this.m_distance_kalman;}

    private double getDistanceDFilter() {return this.m_distance_doublefilter;}

    private double getMediaMovel(){return this.m_rssi_movel;}

    private double getMediaKalman(){return this.m_rssi_kalman;}

    private double getMediaDFilter(){return this.m_rssi_doublefilter;}

    public int getRealRSSI()
    {
        return this.r_rssi;
    }

    public String getName()
    {
        return this.name;
    }

    public String getID()
    {
        return this.id;
    }

    public double getX()
    {
        return this.pos[0];
    }

    public double getY()
    {
        return this.pos[1];
    }

    public double getRadius()
    {
        return this.raio;
    }

    public double getN() {return this.n; }

    public double getRSSI_Cn () { return  this.rssi_Cn;}

    public Beacon(String _name, String _id, int _rssi, int _power,long ts)
    {
        beaconref = AppData.getInstance().database.getReference(_id.substring(8));
        produtos = new ArrayList<>();
        this.name = _name;
        this.id = _id;
        this.r_rssi = _rssi;
        this.power = _power;

        //kalman Filter
        kalman.filter(_rssi);
        this.m_rssi_kalman = kalman.lastMeasure();

        //Media movel filter
        media.update(_rssi);
        this.m_rssi_movel = media.getMedia();

        //DoubleFilter
        dfilter.filter(_rssi);
        this.m_rssi_doublefilter = dfilter.getLastMeasure();
        setBeaconInfo();
        String aux =  id.substring(8,11).toUpperCase();
        this.n = SharedPreferencesManager.getInstance().getFloat("N_" + aux,2.0f);
        this.rssi_Cn = SharedPreferencesManager.getInstance().getFloat("RCN_" + aux, -57.0f);
        if (this.id.equalsIgnoreCase("ebfc31e1bec7")) {
            this.A = -57.7f;
        } else {

            this.A = -53.2f;
        }
        this.pos[0] = SharedPreferencesManager.getInstance().getFloat("X_" + aux,1.0f);
        this.pos[1] = SharedPreferencesManager.getInstance().getFloat("Y_" + aux,1.0f);
        this.raio = SharedPreferencesManager.getInstance().getFloat("R_" + aux,1.0f);
        this.last_ts = ts;

        this.m_distance_kalman = Utilidades.getDistance(this.m_rssi_kalman, this.n,this.A);
        this.m_distance_movel = Utilidades.getDistance(this.m_rssi_movel, this.n,this.A);
        this.m_distance_doublefilter = Utilidades.getDistance(this.m_rssi_doublefilter,this.n,this.A);
    }

    public void setBeaconInfo()
    {
        beaconref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snaps : dataSnapshot.getChildren())
                {
                    Map<String, Object> mapa = (Map<String, Object>) snaps.getValue();
                    produtos.add(new Produto(
                            mapa.get("Nome").toString(),
                            Double.parseDouble(mapa.get("Preco").toString()),
                            mapa.get("Descricao").toString(),
                            mapa.get("Imagem").toString(),
                            mapa.get("ID").toString(),
                            mapa.get("Logo").toString()));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void update(int _rssi, int _power, long ts)
    {
        this.r_rssi = _rssi;
        this.power = _power;
        //kalman Filter
        kalman.filter(_rssi);
        this.m_rssi_kalman = kalman.lastMeasure();

        //Media movel filter
        media.update(_rssi);
        this.m_rssi_movel = media.getMedia();

        //DoubleFilter
        dfilter.filter(_rssi);
        this.m_rssi_doublefilter = dfilter.getLastMeasure();

        this.m_distance_kalman = Utilidades.getDistance(this.m_rssi_kalman, this.n,this.A);
        this.m_distance_movel = Utilidades.getDistance(this.m_rssi_movel, this.n,this.A);
        this.m_distance_doublefilter = Utilidades.getDistance(this.m_rssi_doublefilter,this.n,this.A);
        for (Produto p : produtos)
        {
            p.dist = getDistance(Utilidades.STD_DIST);
        }

        this.last_ts = ts;
    }

    @Override
    public String toString()
    {
        return ("NAMESPACE: " + name + "\nID: " + id +"\nTXPOWER: " + power + "\nXY:" + this.pos[0] + " " + this.pos[1] +"\nRAIO: " + this.raio +"\nRSSI_K: " + m_rssi_kalman+ "\nDISTANCE_K: " + m_distance_kalman);
    }

    public static String getInstanceNSpace(byte[] data)
    {
        StringBuilder sb = new StringBuilder();
        int pl = 18 - 6;
        int offset = pl - 10;
        for(int i=offset;i<pl;i++)
        {
            sb.append(Integer.toHexString(data[i] & 0xFF));
        }
        return sb.toString();
    }

    public static String getInstanceId(byte[] data)
    {
        StringBuilder sb = new StringBuilder();
        int pl = 18;
        int offset = pl - 6;
        for(int i=offset;i<pl;i++)
        {
            String aux = Integer.toHexString(data[i] & 0xFF);
            if(aux.length()==1)
            {
                aux = "0" + aux;
            }
            sb.append(aux);
        }
        return sb.toString();
    }

    public void ClearFilter (){
        media.Clear();
        kalman.Clear(0.7,1.5);
        dfilter.Clear();
    }

}
