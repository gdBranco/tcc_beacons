package unb.beacon.beacon_project.Utilidades;


import android.util.Log;
import android.util.Pair;


/**
 * Created by samuelpala on 10/5/16.
 */
public class Centroid {

    private static final String TAG = Centroid.class.getSimpleName();
    MediaMovel Xpoints = new MediaMovel(10);
    MediaMovel Ypoints = new MediaMovel(10);

    public Centroid() {

    }

    public Pair<Double,Double> getCentroid () {
        Pair<Double,Double> p = new Pair(Xpoints.getMedia(), Ypoints.getMedia());

        return p;
    }

    public void add(double x, double y) {
        /*if (Xpoints.getQSize() >= 2) {
            if ((Ypoints.getInterval().first <= y && Ypoints.getInterval().second >= y) || (Xpoints.getInterval().first <= x && Xpoints.getInterval().second >= x)) {
                Xpoints.update(x);
                Ypoints.update(y);
            } else {
                Log.d(TAG, String.format("Descartado X: %.2f %.2f %.2f", x, Xpoints.getInterval().first, Xpoints.getInterval().second));
                Log.d(TAG, String.format("Descartado Y: %.2f %.2f %.2f", y, Ypoints.getInterval().first, Ypoints.getInterval().second));
            }
        } else {*/
            Xpoints.update(x);
            Ypoints.update(y);
//        }
    }
}
