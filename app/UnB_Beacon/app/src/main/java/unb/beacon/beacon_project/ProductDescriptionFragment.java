package unb.beacon.beacon_project;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import jp.wasabeef.glide.transformations.ColorFilterTransformation;
import jp.wasabeef.glide.transformations.CropSquareTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import unb.beacon.beacon_project.Utilidades.AppData;



/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDescriptionFragment extends Fragment {

    private ImageView image;
    private TextView preco;
    private TextView nome;
    private TextView descricao;
    private ImageView logo;

    public ProductDescriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_description, container, false);

        image = (ImageView) view.findViewById(R.id.profile_image);
        Glide.with(getContext()).load("file:///android_asset/" + AppData.getInstance().pSelected.imagem)
                .bitmapTransform(new CropSquareTransformation(getContext()),new RoundedCornersTransformation(getContext(), 25,10))
                .into(image);
        logo = (ImageView) view.findViewById(R.id.logo_descricao);
        int color;

        if(AppData.getInstance().pSelected.logo.equalsIgnoreCase("orange")) {
            color = Color.rgb(255,165,0);
        }else if(AppData.getInstance().pSelected.logo.equalsIgnoreCase("blue")) {
            color = Color.BLUE;
        }else if(AppData.getInstance().pSelected.logo.equalsIgnoreCase("red")) {
            color = Color.RED;
        } else {
            color = Color.GREEN;
        }

        Glide.with(getContext()).load("file:///android_asset/ic_launcher.png").bitmapTransform(new ColorFilterTransformation(getContext(), color), new RoundedCornersTransformation(getContext(), 10, 1)).into(logo);

        preco = (TextView) view.findViewById(R.id.text_ppreco);
        preco.setText(String.format("R$ %.2f", AppData.getInstance().pSelected.preco));
        nome = (TextView) view.findViewById(R.id.text_pnome);
        nome.setText(AppData.getInstance().pSelected.nome);
        descricao = (TextView) view.findViewById(R.id.text_pdescricao);
        descricao.setText(AppData.getInstance().pSelected.descricao);
        descricao.setMovementMethod(new ScrollingMovementMethod());

        return view;

    }

}
