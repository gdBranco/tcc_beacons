package unb.beacon.beacon_project;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.ArraySet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;


import jp.wasabeef.glide.transformations.ColorFilterTransformation;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import unb.beacon.beacon_project.Utilidades.AppData;
import unb.beacon.beacon_project.Utilidades.Beacon;
import unb.beacon.beacon_project.Utilidades.Produto;
import unb.beacon.beacon_project.Utilidades.Utilidades;

/**
 * Created by samuelpala on 1/4/17.
 */

public class AdapterList_Produto extends BaseAdapter {

    private static final String TAG = AdapterList_Produto.class.getSimpleName();
    private Context context;
    ArrayList<Produto> lista;
    public AdapterList_Produto(Context context, ArrayList<Produto> lista)
    {
        this.context = context;
        this.lista = lista;
    }

    public void removeAll(ArrayList<Produto> mortos)
    {
        lista.removeAll(mortos);
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.lista_produto,null);
        if(convertView!=null)
        {
            Produto produto = lista.get(position);
            ImageView riv = (ImageView) convertView.findViewById(R.id.profile_image);
            Glide.with(context).load("file:///android_asset/" + produto.imagem).bitmapTransform(new CropCircleTransformation(context)).into(riv);
            TextView text_name = (TextView) convertView.findViewById(R.id.produto_nome);
            TextView text_preco = (TextView) convertView.findViewById(R.id.produto_preco);
            ImageView logo = (ImageView) convertView.findViewById(R.id.logo_imagem);
            TextView text_dist = (TextView) convertView.findViewById(R.id.produto_distancia);
            text_dist.setText(String.format("%.2f", produto.dist));
            int color;
            if(produto.logo.equalsIgnoreCase("orange")) {
                color = Color.rgb(255,165,0);
            }else if(produto.logo.equalsIgnoreCase("blue")) {
                color = Color.BLUE;
            }else if(produto.logo.equalsIgnoreCase("red")) {
                color = Color.RED;
            } else {
                color = Color.GREEN;
            }

            Glide.with(context).load("file:///android_asset/ic_launcher.png").bitmapTransform(new ColorFilterTransformation(context, color), new RoundedCornersTransformation(context, 10, 1)).into(logo);
            text_preco.setText(String.format("R$ %.2f", produto.preco));
            text_preco.setTextSize(15.0f);
            text_preco.setPadding(50,5,0,5);
            text_name.setText(produto.nome);
            text_name.setTextSize(20.0f);
            text_name.setTypeface(null, Typeface.BOLD);
            text_name.setPadding(20,5,0,5);

        }
        return convertView;
    }
}
