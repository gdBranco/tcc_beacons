package unb.beacon.beacon_project.Utilidades;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.Toast;

/**
 * Created by samuelpala on 8/30/16.
 */
public class BManager {
    Activity mContext;
    public BManager(Activity Context)
    {
        this.mContext = Context;
    }

    public boolean checkBluetoothStatus()
    {
        BluetoothManager manager = (BluetoothManager) mContext.getSystemService(mContext.BLUETOOTH_SERVICE);
        BluetoothAdapter adapter = manager.getAdapter();
        if(!adapter.isEnabled())
        {
            Intent enablebt = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            mContext.startActivityForResult(enablebt, Utilidades.REQUEST_ENABLE_BLUETOOTH);
            return false;
        }
        if(!mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE))
        {
            Toast.makeText(mContext,"Sem suporte LE", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


/*    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 0: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permissao concedida
                } else {
                    //Permissao negada
                }
                return;
            }
        }
    }*/

/*    public boolean request_local(){
        if (ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(mContext,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(mContext,
                        new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        0);
            }
        }
        return true;
    }*/

}
