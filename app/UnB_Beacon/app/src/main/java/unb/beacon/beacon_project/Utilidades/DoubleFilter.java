package unb.beacon.beacon_project.Utilidades;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by gdbra on 27/10/2016.
 */

public class DoubleFilter {

    private static final String TAG = DoubleFilter.class.getSimpleName();
    private double Ab = 0.7;
    private double Bb = 0.05;
    private double Wn = 0.9;
    private double Ac = 0.95;
    private double Bc = 0.5;
    private double Wm = 1.1;
    private double An;
    private double Bn;
    private double soma = 0;
    private int cont_media = 0;
    private double last=-1;
    private int cont_geral = 0;
    private double second2last = -1;
    DoubleFilter(double _an,double _bn)
    {
        An = _an;
        Bn = _bn;
    }
    private double media_erro(double rssi)
    {
        if(last!=-1) {
            soma += rssi;
            cont_media++;
            return soma / cont_media;
        }
        return 0;
    }
    private void filtro(int rssi)
    {
        double result;
        double erro = media_erro(last);
        if(cont_geral > 1)
            result = (1 - An) * second2last + An * (erro + (rssi - erro) * Bn);
        else
            result = rssi;
        second2last = last;
        last = result;
        cont_geral++;
    }
    public void filter(int rssi)
    {
        int movendo = SharedPreferencesManager.getInstance().getInt(Utilidades.P_MOVINGPHONE);
        if(movendo == 1)
        {
            if((An < Ac) && (Bn < Bc))
            {
                An *= Wm;
                Bn *= Wm;
            }
        }
        else
        {
            if ((An > Ab) && (Bn > Bb))
            {
                An *= Wn;
                Bn *= Wn;
            }
        }
        filtro(rssi);
    }

    public void Clear () {
        soma = 0;
        cont_media = 0;
        last=-1;
        cont_geral = 0;
        second2last = -1;
    }

    public double getLastMeasure()
    {
        return last;
    }

}
