package unb.beacon.beacon_project;


import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import unb.beacon.beacon_project.Utilidades.AppData;
import unb.beacon.beacon_project.Utilidades.CsvGenerator;
import unb.beacon.beacon_project.Utilidades.MediaMovel;
import unb.beacon.beacon_project.Utilidades.ScannerManager;
import unb.beacon.beacon_project.Utilidades.Utilidades;


/**
 * A simple {@link Fragment} subclass.
 */
public class BeaconDescriptionFragment extends Fragment {

    private static final int UI_TASK_PERIOD = 2000;
    private static final String TAG = BeaconDescriptionFragment.class.getSimpleName();

    private TextView num_dist;
    private TextView num_rssi;
    private EditText num_PX;
    private EditText num_PY;
    private TextView hex_name;
    private TextView hex_ID;
    private EditText num_raio;
    private GraphView graph;
    private FloatingActionButton calibrator_btn;
    private Button editar_btn;
    private boolean editar = false;
    private boolean calibrando = false;
    private ProgressBar progress;
    private TextView num_n;
    private TextView num_rssiCn;
    private Switch csv_btn;

    CsvGenerator csv;

    private LineGraphSeries<DataPoint> mSeriesR = new LineGraphSeries<>();
    private LineGraphSeries<DataPoint> mSeriesK = new LineGraphSeries<>();
    private LineGraphSeries<DataPoint> mSeriesM = new LineGraphSeries<>();
    private LineGraphSeries<DataPoint> mSeriesD = new LineGraphSeries<>();
    private int cont = 0;
    private int contCalibragem = 0;
    private boolean calibragem3 = false;
    private int contLog = 1;
    private double rssi1 = 0;
    private double rssi3 = 0;
    String update_bdescription;
    MediaMovel media = new MediaMovel(Utilidades.MAXCALIBRAGEM);
    View view;

    public BeaconDescriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_beacon_description, container, false);

        num_dist = (TextView) view.findViewById(R.id.num_dist);
        num_rssi = (TextView) view.findViewById(R.id.num_rssi);
        num_PX = (EditText) view.findViewById(R.id.num_PX);
        num_PY = (EditText) view.findViewById(R.id.num_PY);
        hex_name = (TextView) view.findViewById(R.id.hex_name);
        hex_ID = (TextView) view.findViewById(R.id.hex_ID);
        graph = (GraphView) view.findViewById(R.id.grafico_rssi);
        num_raio = (EditText) view.findViewById(R.id.num_raio);
        num_n = (TextView) view.findViewById(R.id.num_n);
        num_rssiCn = (TextView) view.findViewById(R.id.num_rssiCn);
        num_PX.setEnabled(false);
        num_PY.setEnabled(false);
        num_raio.setEnabled(false);
        editar_btn = (Button) view.findViewById(R.id.id_editar);
        progress = (ProgressBar) view.findViewById(R.id.id_progress);
        progress.setVisibility(View.INVISIBLE);
        progress.setProgress(0);
        csv_btn = (Switch) view.findViewById(R.id.switch_csv);
        csv_btn.setText("");

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(-90);
        graph.getViewport().setMaxY(-20);
        graph.getViewport().scrollToEnd();

        cont = 0;
        mSeriesK.resetData(new DataPoint[] {new DataPoint(cont, AppData.getInstance().selected.getRSSI(Utilidades.KALMAN))});
        mSeriesM.resetData(new DataPoint[] {new DataPoint(cont, AppData.getInstance().selected.getRSSI(Utilidades.MOVEL))});
        mSeriesD.resetData(new DataPoint[] {new DataPoint(cont, AppData.getInstance().selected.getRSSI(Utilidades.DFILTER))});
        mSeriesR.resetData(new DataPoint[] {new DataPoint(cont++, AppData.getInstance().selected.getRealRSSI())});

        mSeriesR.setColor(Color.GREEN);
        mSeriesK.setColor(Color.RED);
        mSeriesD.setColor(Color.BLUE);
        mSeriesM.setColor(Color.BLACK);
        graph.addSeries(mSeriesM);
        graph.addSeries(mSeriesK);
        graph.addSeries(mSeriesD);
        graph.addSeries(mSeriesR);

        hex_ID.setText(AppData.getInstance().selected.getID());
        hex_name.setText(AppData.getInstance().selected.getName());

        mHandler.post(updateUI);

        return view;
    }

    private Handler mHandler = new Handler();
    private Runnable updateUI = new Runnable() {
        @Override
        public void run() {
            mSeriesM.appendData(new DataPoint(cont, AppData.getInstance().selected.getRSSI(Utilidades.MOVEL)), false, 5);
            mSeriesK.appendData(new DataPoint(cont, AppData.getInstance().selected.getRSSI(Utilidades.KALMAN)), false, 5);
            mSeriesD.appendData(new DataPoint(cont, AppData.getInstance().selected.getRSSI(Utilidades.DFILTER)),false,5);
            mSeriesR.appendData(new DataPoint(cont++, AppData.getInstance().selected.getRealRSSI()), false, 5);
            num_dist.setText(String.format(" %.2f m", AppData.getInstance().selected.getDistance(Utilidades.STD_DIST)));
            num_rssi.setText(String.format(" %.2f dB", AppData.getInstance().selected.getRSSI(Utilidades.STD_RSSI)));
            num_n.setText(String.format("  %.2f", AppData.getInstance().selected.getN()));
            num_raio.setText(String.format("%.2f", AppData.getInstance().selected.getRadius()));
            num_PX.setText(String.format("%.2f", AppData.getInstance().selected.getX()));
            num_PY.setText(String.format("%.2f", AppData.getInstance().selected.getY()));
            num_rssiCn.setText(String.format("  %.2f", AppData.getInstance().selected.getRSSI_Cn()));
            AppData.getInstance().selected.raio = Double.parseDouble(num_raio.getText().toString().replaceAll(",","."));
            mHandler.postDelayed(this, UI_TASK_PERIOD);
            Log.d(TAG, "Fazendo Update UI");
        }
    };

    @Override
    public void onDestroyView() {
        mHandler.removeCallbacks(updateUI);
        super.onDestroyView();
    }
}
