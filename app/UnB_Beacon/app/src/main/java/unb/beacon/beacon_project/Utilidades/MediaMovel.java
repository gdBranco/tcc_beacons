package unb.beacon.beacon_project.Utilidades;

import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;

/**
 * Created by samuelpala on 9/26/16.
 */
public class MediaMovel {

    private static final String TAG = MediaMovel.class.getSimpleName();
    private int janela_size;
    private double sum = 0;
    private ArrayList<Double> q = new ArrayList<>();

    public MediaMovel(int janela_qtd) {
        this.janela_size = janela_qtd;
    }

    public void update(double value)
    {
        q.add(value);
        if(q.size()>this.janela_size) {
            sum-=q.get(0);
            q.remove(0);
        }
        sum+=q.get(q.size()-1);
    }

    public double getMedia()
    {
        return sum/q.size();
    }

    public void Clear()
    {
        sum = 0;
        q.clear();
    }

    public int getQSize() {return q.size();}

    public double getVariance() {
        double mean = getMedia();
        int size = getQSize();
        double temp = 0;

        for(double d : q) {
            temp += (d-mean)*(d-mean);
        }
        return temp/size;
    }

    public double getStdDev() {
        return Math.sqrt(getVariance());
    }

    public Pair<Double,Double> getInterval() {
        return new Pair<>(getMedia()-getStdDev(), getMedia()+getStdDev());
    }

}
