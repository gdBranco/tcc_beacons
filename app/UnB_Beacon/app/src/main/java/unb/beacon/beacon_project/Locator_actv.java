package unb.beacon.beacon_project;

import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import unb.beacon.beacon_project.Utilidades.Beacon;
import unb.beacon.beacon_project.Utilidades.Centroid;
import unb.beacon.beacon_project.Utilidades.CsvGenerator;
import unb.beacon.beacon_project.Utilidades.MediaMovel;
import unb.beacon.beacon_project.Utilidades.SharedPreferencesManager;
import unb.beacon.beacon_project.Utilidades.Utilidades;



public class Locator_actv extends Activity implements
        ServiceConnection,ScannerService.OnBeaconEventListener, SensorEventListener {
    private static final String TAG = Locator_actv.class.getSimpleName();
    private static final int EXPIRED_TIMEOUT = 15000;
    private static final int EXPIRE_TASK_PERIOD = 5000;

    private ScannerService mService;
    private ListView List_view;
    private ArrayList<Beacon> lista_beacons;
    private AdapterList_Beacon arrayAdapter;

    /*LOCATOR_INTERFACE*/
    private TextView x_real;
    private TextView y_real;
    private TextView x_esti;
    private TextView y_esti;

    /*BEACON DESCRIPTION*/
    private Beacon selected;
    private TextView num_dist;
    private TextView num_rssi;
    private EditText num_PX;
    private EditText num_PY;
    private TextView hex_name;
    private TextView hex_ID;
    private EditText num_raio;
    private GraphView graph;
    private boolean selectedView = false;
    private FloatingActionButton calibrator_btn;
    private Button editar_btn;
    private boolean editar = false;
    private boolean calibrando = false;
    private ProgressBar progress;
    private TextView num_n;
    private TextView num_rssiCn;
    private Switch csv_btn;

    CsvGenerator csv;

    private LineGraphSeries<DataPoint> mSeriesR = new LineGraphSeries<>();
    private LineGraphSeries<DataPoint> mSeriesK = new LineGraphSeries<>();
    private LineGraphSeries<DataPoint> mSeriesM = new LineGraphSeries<>();
    private LineGraphSeries<DataPoint> mSeriesD = new LineGraphSeries<>();
    private int cont = 0;
    private int contCalibragem = 0;
    private boolean calibragem3 = false;
    private int contLog = 1;
    private double rssi1 = 0;
    private double rssi3 = 0;
    String update_bdescription;
    MediaMovel media = new MediaMovel(Utilidades.MAXCALIBRAGEM);
    /* ************************************************ */
    private Centroid center = new Centroid();

    /*ACELEROMETRO*/
    private float mLastX, mLastY, mLastZ;
    private boolean mInitialized;
    private SensorManager mSensorManager;
    private Sensor mAccel;
    private final float NOISE = 3.3f;
    private boolean moving = false;

    /* */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lista_beacons = new ArrayList<>();
        Interface();
    }

    private void Interface()
    {
        setContentView(R.layout.activity_locator);
        /*LOCATOR_INTERFACE*/
        x_real = (TextView) findViewById(R.id.text_x_real);
        y_real = (TextView) findViewById(R.id.text_y_real);
        x_esti = (TextView) findViewById(R.id.text_x_esti);
        y_esti = (TextView) findViewById(R.id.text_y_esti);
        List_view = (ListView) findViewById(R.id.list);

        arrayAdapter = new AdapterList_Beacon(this, lista_beacons);
        List_view.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
        csv = new CsvGenerator(getApplicationContext());
        mInitialized = false;
        mSensorManager = (SensorManager) getSystemService(getApplicationContext().SENSOR_SERVICE);
        mAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        List_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedView = true;

                selected = (Beacon) arrayAdapter.getItem(position);

                setContentView(R.layout.descricao_beacon);

                 /*BEACON DESCRIPTION*/
                num_dist = (TextView)findViewById(R.id.num_dist);
                num_rssi = (TextView)findViewById(R.id.num_rssi);
                num_PX = (EditText) findViewById(R.id.num_PX);
                num_PY = (EditText) findViewById(R.id.num_PY);
                hex_name = (TextView)findViewById(R.id.hex_name);
                hex_ID = (TextView)findViewById(R.id.hex_ID);
                graph = (GraphView) findViewById(R.id.grafico_rssi);
                num_raio = (EditText) findViewById(R.id.num_raio);
                num_n = (TextView) findViewById(R.id.num_n);
                num_rssiCn = (TextView) findViewById(R.id.num_rssiCn);
                num_PX.setEnabled(false);
                num_PY.setEnabled(false);
                num_raio.setEnabled(false);
                editar_btn = (Button) findViewById(R.id.id_editar);
                progress = (ProgressBar) findViewById(R.id.id_progress);
                progress.setVisibility(View.INVISIBLE);
                progress.setProgress(0);
                csv_btn = (Switch) findViewById(R.id.switch_csv);
                csv_btn.setText("");
                if(csv_btn!=null)
                {
                    csv_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                            if (bChecked) {
                                contLog = 1;
                                selected.ClearFilter();
                            } else {
                                csv_btn.setText("");
                            }
                        }
                    });
                }
                if(editar_btn!=null) {
                    editar_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!editar) {
                                if(!calibrando) {
                                    editar = true;
                                    Pause();
                                    num_PX.setEnabled(true);
                                    num_PY.setEnabled(true);
                                    num_raio.setEnabled(true);
                                    editar_btn.setText("Ok");
                                }
                                else
                                {
                                    Toast.makeText(getApplicationContext(), "Aguarde o termino da calibragem!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                String aux = num_raio.getText().toString().replaceAll(",",".");
                                if ((!aux.isEmpty()) && (!aux.equals(""))) {
                                    try {
                                        selected.raio = Double.parseDouble(aux);
                                    } catch (NumberFormatException e) {
                                        num_raio.setError("Formato Incorreto");
                                    }
                                }
                                aux = num_PX.getText().toString().replaceAll(",",".");
                                if ((!aux.isEmpty()) && (!aux.equals(""))) {
                                    try {
                                        selected.pos[0] = Double.parseDouble(aux);
                                    } catch (NumberFormatException e) {
                                        num_PX.setError("Formato Incorreto");
                                    }
                                }
                                aux = num_PY.getText().toString().replaceAll(",",".");
                                if ((!aux.isEmpty()) && (!aux.equals(""))) {
                                    try {
                                        selected.pos[1] = Double.parseDouble(aux);
                                    } catch (NumberFormatException e) {
                                        num_PY.setError("Formato Incorreto");
                                    }
                                }
                                editar = false;
                                aux = selected.getID().substring(8,11).toUpperCase();
                                SharedPreferencesManager.getInstance().putFloat("X_" + aux, (float)selected.getX());
                                SharedPreferencesManager.getInstance().putFloat("Y_" + aux, (float)selected.getY());
                                SharedPreferencesManager.getInstance().putFloat("R_" + aux, (float)selected.getRadius());
                                num_PX.setEnabled(false);
                                num_PY.setEnabled(false);
                                num_raio.setEnabled(false);
                                editar_btn.setText("Editar");
                                Resume();
                            }
                        }
                    });
                }


                num_dist.setText(String.format(" %.2f m",selected.getDistance(Utilidades.STD_DIST)));
                num_rssi.setText(String.format(" %.2f dB",selected.getRSSI(Utilidades.STD_RSSI)));
                num_PX.setText(String.format("%.2f",selected.getX()));
                num_PY.setText(String.format("%.2f",selected.getY()));
                num_raio.setText(String.format("%.2f",selected.getRadius()));
                hex_ID.setText(selected.getID());
                hex_name.setText(selected.getName());
                num_n.setText(String.format("  %.2f", selected.getN()));
                num_rssiCn.setText(String.format("  %2f", selected.getRSSI_Cn()));

                selected.raio = Double.parseDouble(num_raio.getText().toString().replaceAll(",","."));

                graph.getViewport().setYAxisBoundsManual(true);
                graph.getViewport().setMinY(-90);
                graph.getViewport().setMaxY(-20);
                graph.getViewport().scrollToEnd();


                update_bdescription = selected.getID();
                cont = 0;
                mSeriesK.resetData(new DataPoint[] {new DataPoint(cont, selected.getRSSI(Utilidades.KALMAN))});
                mSeriesM.resetData(new DataPoint[] {new DataPoint(cont, selected.getRSSI(Utilidades.MOVEL))});
                mSeriesD.resetData(new DataPoint[] {new DataPoint(cont, selected.getRSSI(Utilidades.DFILTER))});
                mSeriesR.resetData(new DataPoint[] {new DataPoint(cont++, selected.getRealRSSI())});

                mSeriesR.setColor(Color.GREEN);
                mSeriesK.setColor(Color.RED);
                mSeriesD.setColor(Color.BLUE);
                mSeriesM.setColor(Color.BLACK);
                graph.addSeries(mSeriesM);
                graph.addSeries(mSeriesK);
                graph.addSeries(mSeriesD);
                graph.addSeries(mSeriesR);

                calibrator_btn = (FloatingActionButton) findViewById(R.id.calibrator_fab);
                if(calibrator_btn != null) {
                    calibrar();
                }

            }
        });

    }

    public void calibrar() {
        calibrator_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!editar) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(Locator_actv.this);
                    builder.setTitle("Calibrar Dispositivo");
                    builder.setMessage("Fique a 1(um) metro do Beacon e pressione Iniciar!");
                    Pause();

                    progress.setVisibility(View.VISIBLE);
                    builder.setPositiveButton("Iniciar", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            Resume();
                            contCalibragem = 0;
                            selected.ClearFilter();
                            calibrando = true;
                            Runnable r = new Runnable() {
                                @Override
                                public void run() {

                                    while (contCalibragem < Utilidades.MAXCALIBRAGEM)
                                    {
                                        media.update(selected.getRSSI(Utilidades.STD_RSSI));
                                    }
                                    calibrando = false;
                                    calibragem3 = true;
                                    rssi1 = media.getMedia();
                                    media.Clear();
                                }
                            };
                            Thread t = new Thread(r);
                            t.start();
                        }
                    });

                    builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progress.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(), "Calibragem Cancelada", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    Toast.makeText(getApplicationContext(), "Confirme as modificações!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void c3() {
            final AlertDialog.Builder builder = new AlertDialog.Builder(Locator_actv.this);
            builder.setTitle("Calibrar Dispositivo");
            builder.setMessage("Fique a 3(tres) metros do Beacon e pressione Continuar!");
            Pause();

            progress.setVisibility(View.VISIBLE);

            builder.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int id) {
                    selected.ClearFilter();
                    calibrando = true;
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            Resume();
                            while (contCalibragem < Utilidades.MAXCALIBRAGEM*2)
                            {
                                media.update(selected.getRSSI(Utilidades.STD_RSSI));
                            }
                            calibrando = false;
                            rssi3 = media.getMedia();
                            media.Clear();
                            selected.n = Utilidades.calibragem(rssi1, rssi3);
                            String aux = selected.getID().substring(8,11).toUpperCase();
                            SharedPreferencesManager.getInstance().putFloat("N_" + aux,(float)selected.n);
                            SharedPreferencesManager.getInstance().putFloat("RCN_" + aux,(float)rssi1);
                            selected.A = rssi1;
                        }
                    };
                    Thread t = new Thread(r);
                    t.start();
                }
            });

            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    progress.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Calibragem Cancelada", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Resume();
    }

    private void Resume() {
        Intent intent = new Intent(this, ScannerService.class);
        bindService(intent, this, BIND_AUTO_CREATE);
        mSensorManager.registerListener(this, mAccel, SensorManager.SENSOR_DELAY_NORMAL);
        mHandler.post(task_RMexpired);
    }

    @Override
    public void onBackPressed() {
        if (editar) {
            Toast.makeText(getApplicationContext(), "Confirme as modificações!", Toast.LENGTH_SHORT).show();
        } else if (calibrando) {
            Toast.makeText(getApplicationContext(), "Aguarde o termino da calibragem!", Toast.LENGTH_SHORT).show();
        }else {
            if (selectedView) {
                selectedView = false;
                Interface();
            } else {
                finish();
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        Pause();
    }
    private void Pause() {
        mHandler.removeCallbacks(task_RMexpired);
        mService.setBeaconEventListener(null);
        unbindService(this);
        mSensorManager.unregisterListener(this);
    }
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "Conectado ao serviço scanner");
        mService = ((ScannerService.LocalBinder) service).getService();
        mService.setBeaconEventListener(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.d(TAG, "Desconectado do serviço scanner");
        mService = null;
    }

    private Handler mHandler = new Handler();
    private Runnable task_RMexpired = new Runnable() {
        @Override
        public void run() {
            final ArrayList<Beacon> mortos = new ArrayList<>();
            final long now = System.currentTimeMillis();
            for (Beacon b : lista_beacons) {
                long delta = now - b.last_ts;
                if (delta >= EXPIRED_TIMEOUT) {
                    mortos.add(b);
                }
            }
            if (!mortos.isEmpty()) {
                Log.d(TAG, "Achei " + mortos.size() + "beacons expirados");
                arrayAdapter.removeAll(mortos);
                arrayAdapter.notifyDataSetChanged();
            }
            mHandler.postDelayed(this, EXPIRE_TASK_PERIOD);
        }
    };

    @Override
    public void onBeaconIdentifier(String name, String id, Integer rssi, Integer power) {
        //Atualiza os Beacons
        AtualizaBeacons(name,id,rssi,power);
        //Ordena por distancia
        Collections.sort(lista_beacons, new Comparator<Beacon>() {
            @Override
            public int compare(Beacon lhs, Beacon rhs) {
                return (Double.valueOf(lhs.getDistance(Utilidades.STD_DIST)).compareTo(rhs.getDistance(Utilidades.STD_DIST)));
            }
        });
        //Trilateracao se a lista tiver 3 ou mais
        if(lista_beacons.size() > 2) {
            Trilateriza(lista_beacons.get(0), lista_beacons.get(1), lista_beacons.get(2));
        }

    }

    private void Trilateriza(Beacon b1, Beacon b2, Beacon b3)
    {

        double[] posEst =  Utilidades.trilaterar(b1.pos, b2.pos, b3.pos,
                b1.getDistance(Utilidades.STD_DIST), b2.getDistance(Utilidades.STD_DIST), b3.getDistance(Utilidades.STD_DIST));
        double[] posReal = Utilidades.trilaterar(b1.pos, b2.pos, b3.pos,
                b1.raio, b2.raio, b3.raio);

        x_real.setText(String.format("Real X: %.2f",posReal[0]));
        y_real.setText(String.format("Real Y: %.2f",posReal[1]));
        center.add(posEst[0], posEst[1]);
        double xEst = center.getCentroid().first;
        double yEst = center.getCentroid().second;
        x_esti.setText(String.format("Estim X: %.2f", xEst));
        y_esti.setText(String.format("Estim Y: %.2f",yEst));

        String data = String.format("%.2f;%.2f;%.2f;%.2f;%.2f\n", b1.getRSSI(Utilidades.KALMAN), b2.getRSSI(Utilidades.KALMAN), b3.getRSSI(Utilidades.KALMAN), xEst, yEst);
        csv.update(data, "Trilaterar");
    }

    private void AtualizaBeacons(String name, String id, Integer rssi, Integer power)
    {
        final long now = System.currentTimeMillis();
        for (Beacon b : lista_beacons)
        {
            if (id.equalsIgnoreCase(b.id))
            {
                b.update(rssi, power, now);
                arrayAdapter.notifyDataSetChanged();
                if(b.id.equalsIgnoreCase(update_bdescription))
                {
                    //Atualiza descrição do selecionado
                    mSeriesM.appendData(new DataPoint(cont, b.getRSSI(Utilidades.MOVEL)), false, 5);
                    mSeriesK.appendData(new DataPoint(cont, b.getRSSI(Utilidades.KALMAN)), false, 5);
                    mSeriesD.appendData(new DataPoint(cont, b.getRSSI(Utilidades.DFILTER)),false,5);
                    mSeriesR.appendData(new DataPoint(cont++, b.getRealRSSI()), false, 5);
                    num_dist.setText(String.format(" %.2f m",b.getDistance(Utilidades.STD_DIST)));
                    num_rssi.setText(String.format(" %.2f dB",b.getRSSI(Utilidades.STD_RSSI)));
                    num_n.setText(String.format("  %.2f",b.getN()));
                    num_raio.setText(String.format("%.2f", b.getRadius()));
                    num_PX.setText(String.format("%.2f", b.getX()));
                    num_PY.setText(String.format("%.2f", b.getY()));
                    num_rssiCn.setText(String.format("  %.2f", b.getRSSI_Cn()));

                    if (csv_btn.isChecked()) {
                        String data = String.format("%d;%.2f;%.2f;%.2f\n",b.getRealRSSI(),b.getRSSI(Utilidades.MOVEL),b.getRSSI(Utilidades.KALMAN),b.getRSSI(Utilidades.DFILTER));
                        csv.update(data, String.format("%s_%.0fm",b.id,b.raio));
                        csv_btn.setText(String.format("%d",contLog++));
                    }

                    //Se estiver calibrando fazer update da calibragem
                    if (calibrando)
                    {
                        contCalibragem++;
                        progress.setProgress((contCalibragem/2)*Utilidades.RATIOCALIBRAGEM);
                        if (contCalibragem == Utilidades.MAXCALIBRAGEM*2)
                        {
                            /*ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                            toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP,1000);*/
                            Toast.makeText(getApplicationContext(), "Calibragem Concluida!", Toast.LENGTH_SHORT).show();

                            progress.setVisibility(View.INVISIBLE);
                        }
                    }
                    if (calibragem3) {
                        calibragem3 = false;
                        c3();
                    }
                }
                return;
            }
        }

        lista_beacons.add(new Beacon(name, id, rssi.intValue(), power.intValue(), now));
        arrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // can be safely ignored for this demo
    }

    private long lastUpdate = 0;

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        long curTime = System.currentTimeMillis();
        if((curTime - lastUpdate) > 500){
            long diffTime = (curTime - lastUpdate);
            lastUpdate = curTime;
            float speed = Math.abs(x+y+z - mLastX - mLastY - mLastZ) / diffTime * 10000;
            if (!mInitialized) {
                mLastX = x;
                mLastY = y;
                mLastZ = z;
                mInitialized = true;
            } else {
                if (speed > NOISE)
                {
                    //x_real.setText("Movendo");
                    moving = true;
                    SharedPreferencesManager.getInstance().putInt(Utilidades.P_MOVINGPHONE,1);
                } else {
                    //x_real.setText("Parado");
                    moving = false;
                    SharedPreferencesManager.getInstance().putInt(Utilidades.P_MOVINGPHONE,0);
                }
                mLastX = x;
                mLastY = y;
                mLastZ = z;
            }
        }
    }
}