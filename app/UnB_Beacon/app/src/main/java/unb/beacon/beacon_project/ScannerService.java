package unb.beacon.beacon_project;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import unb.beacon.beacon_project.Utilidades.Beacon;
import unb.beacon.beacon_project.Utilidades.Utilidades;

/**
 * Created by samuelpala on 8/30/16.
 */
public class ScannerService extends Service {

    private static final String TAG = ScannerService.class.getSimpleName();
    private BluetoothLeScanner mBluetoothScanner;
    private OnBeaconEventListener mBeaconEventListener;
    private int cont = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        mBluetoothScanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();
        startDiscovery();
    }

    @Override
    public void onDestroy() {
        stopDiscovery();
    }

    public interface OnBeaconEventListener
    {
        void onBeaconIdentifier(String name, String id, Integer rssi, Integer power);
    }

    public void setBeaconEventListener(OnBeaconEventListener listener)
    {
        mBeaconEventListener = listener;
    }

    private LocalBinder mBinder = new LocalBinder();
    public class LocalBinder extends Binder {
        public ScannerService getService()
        {
            return ScannerService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void startDiscovery() {

        List<ScanFilter> filters = new ArrayList<>();
        ScanFilter filter = new ScanFilter.Builder()
                .setServiceUuid(Utilidades.SERVICE_UUID)
                .build();
        filters.add(filter);

        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build();

        mBluetoothScanner.startScan(filters, settings, scanCallback);

    }

    private void stopDiscovery()
    {
        mBluetoothScanner.stopScan(scanCallback);
    }

    private void processEddyUID(String name, String id, Integer rssi, Integer power)
    {
        if(mBeaconEventListener != null)
        {
            mBeaconEventListener.onBeaconIdentifier(name,id,rssi,power);
        }
    }

    private ScanCallback scanCallback = new ScanCallback() {
        private Handler CallbackHandler = new Handler(Looper.getMainLooper());

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            processResult(result);
        }

        @Override
        public void onScanFailed(int errorCode)
        {
            Log.w(TAG,"Scan Error Code: " + errorCode);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results)
        {
            for(ScanResult result : results)
            {
                processResult(result);
            }
        }

        private void processResult(ScanResult result) {

            byte[] data = result.getScanRecord().getServiceData(Utilidades.SERVICE_UUID);

            if (data == null) {
                return;
            }

            byte frametype = data[0];

            switch (frametype) {
                case Utilidades.FRAME_TYPE_UID:
                    final Integer power = new Byte(data[1]).intValue();
                    final String name;
                    final String id;
                    name = Beacon.getInstanceNSpace(data);
                    id = Beacon.getInstanceId(data);
                    final Integer rssi = new Integer(result.getRssi());
//                    Log.d(TAG,String.format("%d : %d",cont++,rssi));
                    CallbackHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            processEddyUID(name, id, rssi, power);
                        }
                    });
                    break;
            }
        }
    };
}
