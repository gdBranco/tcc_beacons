package unb.beacon.beacon_project.Utilidades;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileWriter;

/**
 * Created by samuelpala on 10/20/16.
 */
public class CsvGenerator {
    File basedir;
    String filePath;
    FileWriter f;
    private static final String TAG = CsvGenerator.class.getSimpleName();



    public CsvGenerator (Context appContext) {
        String path = Environment.getExternalStorageDirectory().getPath() + File.separator + "Download" + File.separator + "Beacon" + File.separator +"Outputs";
        basedir = new File(path);
        basedir.mkdirs();

    }

    public void update (String _data, String _name) {
       filePath = basedir.getPath() + "/" + _name + ".csv";
        try {
            f = new FileWriter(filePath,true);
            f.append(_data);
            f.close();
        } catch (Exception e) {

        }
    }
}
