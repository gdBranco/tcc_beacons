package unb.beacon.beacon_project.Utilidades;

import android.util.Pair;

import java.util.ArrayList;

/**
 * Created by samuelpala on 10/6/16.
 */
public class VarianciaMovel {
   double temp;
    double media;
    int janelaSize;
    private ArrayList<Double> q = new ArrayList<>();

    public VarianciaMovel(int _size, double _media) {
        this.janelaSize = _size;
        this.media = _media;
    }

    public double getVariance() {

        return temp/q.size();
    }
    public void update(double _media, double value){
        this.media = _media;
        q.add(value);
        if (q.size() > janelaSize) {

            temp -= (q.get(0)-media)*(q.get(0)-media);
            q.remove(0);
        }
        temp += (q.get(0)-media)*(q.get(0)-media);
    }
    double getStdDev() {
        return Math.sqrt(getVariance());
    }


}

