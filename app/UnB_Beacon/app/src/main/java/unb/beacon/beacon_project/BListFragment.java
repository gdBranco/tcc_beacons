package unb.beacon.beacon_project;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import unb.beacon.beacon_project.Utilidades.Beacon;
import unb.beacon.beacon_project.Utilidades.AppData;

/**
 * A simple {@link Fragment} subclass.
 */
public class BListFragment extends Fragment {

    private static final String TAG = BListFragment.class.getSimpleName();
    ListView listView;

    public BListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_blist, container, false);

        listView = (ListView) view.findViewById(R.id.list);

        listView.setAdapter(AppData.getInstance().adapter);
        AppData.getInstance().adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AppData.getInstance().selected = (Beacon) AppData.getInstance().adapter.getItem(position);
                FragmentManager manager = getFragmentManager();
                FragmentTransaction fragmentTransaction = manager.beginTransaction();

                BeaconDescriptionFragment beaconDescriptionFragment = new BeaconDescriptionFragment();
                fragmentTransaction.add(R.id.content_main_, beaconDescriptionFragment, beaconDescriptionFragment.getTag());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

}
