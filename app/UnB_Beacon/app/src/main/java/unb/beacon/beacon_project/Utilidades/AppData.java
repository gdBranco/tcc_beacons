package unb.beacon.beacon_project.Utilidades;

import android.content.Context;
import android.os.Handler;
import android.util.ArraySet;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Set;

import unb.beacon.beacon_project.AdapterList_Beacon;
import unb.beacon.beacon_project.AdapterList_Produto;
import unb.beacon.beacon_project.MainActivity;

/**
 * Created by samuelpala on 1/3/17.
 */

public class AppData {

    private static final String TAG = AppData.class.getSimpleName();
    private static final int UI_TASK_PERIOD = 2000;
    private static AppData instance = null;
    public ArrayList<Beacon> lista_beacons;
    public AdapterList_Beacon adapter;
    public Beacon selected;
    public Produto pSelected;
    public ArrayList<Produto> setProdutos;
    public AdapterList_Produto arrayAdapter;
    public Handler mHandler = new Handler();
    public Handler mHandler2 = new Handler();
    public boolean running = true;
    public double MAX_PROXIMIDADE = 2.0d;
    public FirebaseDatabase database = FirebaseDatabase.getInstance();

    public static AppData getInstance()
    {
        if(instance==null)
        {
            instance = new AppData();
        }
        return instance;
    }

    public void Init(Context context)
    {
        lista_beacons = new ArrayList<>();
        adapter = new AdapterList_Beacon(context, lista_beacons);
        setProdutos = new ArrayList<>();
        arrayAdapter = new AdapterList_Produto(context, setProdutos);
        mHandler.post(task_RMexpired);
        mHandler2.post(updateUI);
    }

    public Runnable task_RMexpired = new Runnable() {
        @Override
        public void run() {
            final ArrayList<Beacon> mortos = new ArrayList<>();
            final long now = System.currentTimeMillis();
            for (Beacon b : lista_beacons) {
                long delta = now - b.last_ts;
                if (delta >= Utilidades.EXPIRED_TIMEOUT) {
                    mortos.add(b);
                }
            }
            if (!mortos.isEmpty()) {
                Log.d(TAG, "Achei " + mortos.size() + " beacons expirados");
                lista_beacons.removeAll(mortos);
                for (Beacon m : mortos) {
                    setProdutos.removeAll(m.produtos);
                }
                adapter.notifyDataSetChanged();
                arrayAdapter.notifyDataSetChanged();
            }
            mHandler.postDelayed(this, Utilidades.EXPIRE_TASK_PERIOD);
        }
    };

    public Runnable updateUI = new Runnable() {
        @Override
        public void run() {

            for (Beacon b : lista_beacons) {
                if(b.getDistance(Utilidades.STD_DIST) > MAX_PROXIMIDADE)
                {
                    setProdutos.removeAll(b.produtos);
                }
                else
                {
                    for(Produto p : b.produtos) {
                        if(!contido(p)) {
                            setProdutos.add(p);
                        }
                    }
                }
            }
            arrayAdapter.notifyDataSetChanged();
            mHandler2.postDelayed(this, UI_TASK_PERIOD);
        }
    };

    private boolean contido (Produto p) {
        for(Produto p2 : setProdutos) {
            if(p.nome.equalsIgnoreCase(p2.nome)) {
                return true;
            }
        }
        return false;
    }
}
