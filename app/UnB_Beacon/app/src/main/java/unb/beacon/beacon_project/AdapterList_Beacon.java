package unb.beacon.beacon_project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import unb.beacon.beacon_project.Utilidades.Beacon;
import unb.beacon.beacon_project.Utilidades.Utilidades;

/**
 * Created by gdbra on 24/08/2016.
 */
public class AdapterList_Beacon extends BaseAdapter {

    private Context context;
    ArrayList<Beacon> lista;
    public AdapterList_Beacon(Context context, ArrayList<Beacon> lista)
    {
        this.context = context;
        this.lista = lista;
    }

    public void removeAll(ArrayList<Beacon> mortos)
    {
        lista.removeAll(mortos);
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.lista_beacon,null);
        if(convertView!=null)
        {
            TextView text_dist = (TextView) convertView.findViewById(R.id.textdist);
            TextView text_rssi = (TextView) convertView.findViewById(R.id.textRSSI);
            TextView text_name = (TextView) convertView.findViewById(R.id.text_name);
            TextView text_id = (TextView) convertView.findViewById(R.id.text_ID);
            Beacon beacon = lista.get(position);
            text_dist.setText(String.format("%.2f m",beacon.getDistance(Utilidades.STD_DIST)));
            text_rssi.setText(String.format("%d db",(int)beacon.getRSSI(Utilidades.STD_RSSI)));
            text_name.setText(beacon.getName());
            text_id.setText(beacon.getID());
        }
        return convertView;
    }
}
