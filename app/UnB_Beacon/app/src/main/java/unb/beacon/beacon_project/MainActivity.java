package unb.beacon.beacon_project;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Collections;
import java.util.Comparator;

import unb.beacon.beacon_project.Utilidades.AppData;
import unb.beacon.beacon_project.Utilidades.Produto;
import unb.beacon.beacon_project.Utilidades.ScannerManager;
import unb.beacon.beacon_project.Utilidades.SharedPreferencesManager;
import unb.beacon.beacon_project.Utilidades.Utilidades;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ToggleButton locatorToggle;
    private ListView lista;
    private Spinner filtro;
    BluetoothAdapter Badapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SharedPreferencesManager.getInstance(getApplicationContext());
        AppData.getInstance();
        AppData.getInstance().Init(this);
        ScannerManager.getInstance(this);
        request_permissions();
        Badapter = BluetoothAdapter.getDefaultAdapter();

        if(!Badapter.isEnabled())
        {
            if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE))
            {
                Toast.makeText(getApplicationContext(),"Nao há suporte BLE", Toast.LENGTH_LONG).show();
                System.exit(-1);
            }
            Intent enablebt = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enablebt, Utilidades.REQUEST_ENABLE_BLUETOOTH);
        }

        lista = (ListView) findViewById(R.id.lista_produtos);
        lista.setAdapter(AppData.getInstance().arrayAdapter);
        AppData.getInstance().arrayAdapter.notifyDataSetChanged();
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AppData.getInstance().pSelected = (Produto) AppData.getInstance().arrayAdapter.getItem(position);
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = manager.beginTransaction();

                ProductDescriptionFragment productDescriptionFragment = new ProductDescriptionFragment();
                fragmentTransaction.add(R.id.content_main_, productDescriptionFragment, productDescriptionFragment.getTag());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        locatorToggle = (ToggleButton) findViewById(R.id.toggle_locator);
        locatorToggle.setTextOff("Parado");
        locatorToggle.setTextOn("Scanning");
        locatorToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ScannerManager.getInstance().Resume();
                } else {
                    ScannerManager.getInstance().Pause();
                }
            }
        });

        filtro = (Spinner) findViewById(R.id.spinner_filter);
        ArrayAdapter<CharSequence> array = ArrayAdapter.createFromResource(getBaseContext(),R.array.array_filtro, android.R.layout.simple_spinner_dropdown_item);
        array.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        filtro.setAdapter(array);
        filtro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = (String) parent.getItemAtPosition(position);
                if(selected.equalsIgnoreCase("Preço")) {
                    Collections.sort(AppData.getInstance().setProdutos, new Comparator<Produto>() {
                        @Override
                        public int compare(Produto lhs, Produto rhs) {
                            return (Double.valueOf(lhs.preco).compareTo(rhs.preco));
                        }
                    });
                } else {
                    Collections.sort(AppData.getInstance().setProdutos, new Comparator<Produto>() {
                        @Override
                        public int compare(Produto lhs, Produto rhs) {
                            return (Double.valueOf(lhs.dist).compareTo(rhs.dist));
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // NOP
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(locatorToggle.isActivated()) {
                ScannerManager.getInstance().Pause();
                locatorToggle.setChecked(false);
            }
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main_, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();

        if (id == R.id.nav_broad) {
            BroadcastFragment broadcastFragment = new BroadcastFragment();
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentTransaction.add(R.id.content_main_, broadcastFragment, broadcastFragment.getTag());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_blist) {
            BListFragment bListFragment = new BListFragment();
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentTransaction.add(R.id.content_main_, bListFragment, bListFragment.getTag());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_manage) {
            ToolsFragment toolsFragment = new ToolsFragment();
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentTransaction.add(R.id.content_main_, toolsFragment, toolsFragment.getTag());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    public void onDestroy() {
        AppData.getInstance().mHandler.removeCallbacks(AppData.getInstance().task_RMexpired);
        AppData.getInstance().mHandler2.removeCallbacks(AppData.getInstance().updateUI);
        super.onDestroy();
    }


    public void request_permissions(){
        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Utilidades.REQUEST_PERM);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utilidades.REQUEST_PERM:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(getApplicationContext(), "Permissao concedida", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(getApplicationContext(), "Permissao negada", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == Utilidades.REQUEST_ENABLE_BLUETOOTH) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"Bluetooth Ativado", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
