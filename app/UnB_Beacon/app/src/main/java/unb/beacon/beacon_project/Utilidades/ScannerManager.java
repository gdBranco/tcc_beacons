package unb.beacon.beacon_project.Utilidades;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import java.util.Collections;
import java.util.Comparator;

import unb.beacon.beacon_project.ScannerService;

/**
 * Created by samuelpala on 1/3/17.
 */

public class ScannerManager implements ServiceConnection,ScannerService.OnBeaconEventListener {

    private static final String TAG = ScannerManager.class.getSimpleName();
    private ScannerService mService;
    private static Context context;
    private static ScannerManager instance = null;

    public static ScannerManager getInstance(Context _context) {
        if(instance==null)
        {
            instance = new ScannerManager();
            context = _context;
        }
        return instance;
    }

    public static ScannerManager getInstance() {
        if(instance!=null)
        {
            return instance;
        }
        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");
    }

    public void Resume() {
        Intent intent = new Intent(context, ScannerService.class);
        context.bindService(intent, this, context.BIND_AUTO_CREATE);
    }

    public void Pause() {
        mService.setBeaconEventListener(null);
        context.unbindService(this);
    }
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "Conectado ao serviço scanner");
        mService = ((ScannerService.LocalBinder) service).getService();
        mService.setBeaconEventListener(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.d(TAG, "Desconectado do serviço scanner");
        mService = null;
    }
    @Override
    public void onBeaconIdentifier(String name, String id, Integer rssi, Integer power) {
        //Atualiza os Beacons
        AtualizaBeacons(name,id,rssi,power);
        Log.d(TAG, "Recebido " + id);
        //Ordena por distancia
        Collections.sort(AppData.getInstance().lista_beacons, new Comparator<Beacon>() {
            @Override
            public int compare(Beacon lhs, Beacon rhs) {
                return (Double.valueOf(lhs.getDistance(Utilidades.STD_DIST)).compareTo(rhs.getDistance(Utilidades.STD_DIST)));
            }
        });

    }

    private void AtualizaBeacons(String name, String id, Integer rssi, Integer power)
    {
        final long now = System.currentTimeMillis();
        for (Beacon b : AppData.getInstance().lista_beacons)
        {
            if (id.equalsIgnoreCase(b.id))
            {
                b.update(rssi, power, now);
                AppData.getInstance().adapter.notifyDataSetChanged();
                return;
            }
        }
        AppData.getInstance().lista_beacons.add(new Beacon(name, id, rssi.intValue(), power.intValue(), now));
        AppData.getInstance().adapter.notifyDataSetChanged();

    }

}
