package unb.beacon.beacon_project.Utilidades;

import android.content.SharedPreferences;
import android.content.Context;

/**
 * Created by samuelpala on 8/30/16.
 */
public class SharedPreferencesManager {

    public static final String SHARED_PREFS_NAME = "beacon-uid-prefs";
    private static SharedPreferencesManager instance = null;
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;


    private SharedPreferencesManager(Context context) {
        mPref = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferencesManager getInstance(Context context)
    {
        if(instance==null)
        {
            instance = new SharedPreferencesManager(context);
        }
        return instance;
    }

    public static SharedPreferencesManager getInstance()
    {
        if(instance!=null)
        {
            return instance;
        }
        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public String getString(String key, String defaultValue)
    {
        return mPref.getString(key, defaultValue);
    }

    public String getString(String key)
    {
        return mPref.getString(key,null);
    }

    public int getInt(String key, int defaultValue)
    {
        return mPref.getInt(key,defaultValue);
    }

    public int getInt(String key)
    {
        return mPref.getInt(key,0);
    }

    public void putString(String key, String value)
    {
        doEdit();
        mEditor.putString(key,value);
        doCommit();

    }

    public void putInt(String key, int value)
    {
        doEdit();
        mEditor.putInt(key,value);
        doCommit();
    }

    public void putFloat(String key, float value) {
        doEdit();
        mEditor.putFloat(key, value);
        doCommit();
    }

    public float getFloat (String key, float defaultValue) { return mPref.getFloat(key, defaultValue);}
    public float getFloat (String key) {
        return mPref.getFloat(key, 0.0f);
    }

    public void clear()
    {
        doEdit();
        mEditor.clear();
        doCommit();
    }

    private void doEdit()
    {
        if(mEditor == null)
        {
            mEditor = mPref.edit();
        }
    }

    private void doCommit()
    {
        if(mEditor != null)
        {
            mEditor.commit();
            mEditor = null;
        }
    }
}
