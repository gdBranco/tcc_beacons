package unb.beacon.beacon_project;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import unb.beacon.beacon_project.R;
import unb.beacon.beacon_project.Utilidades.AppData;

/**
 * A simple {@link Fragment} subclass.
 */
public class ToolsFragment extends Fragment {
    private final static String TAG = ToolsFragment.class.getSimpleName();


    public ToolsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tools, container, false);

        Switch debug = (Switch) view.findViewById(R.id.debug_switch);
        SeekBar proximidade = (SeekBar) view.findViewById(R.id.proximidade_seekbar);
        final TextView proxSeekBar = (TextView) view.findViewById(R.id.text_proximidade);
        proxSeekBar.setText(String.format("%.2f",AppData.getInstance().MAX_PROXIMIDADE));

        proximidade.setMax(9);
        proximidade.setProgress((int)AppData.getInstance().MAX_PROXIMIDADE*2);
        proximidade.incrementProgressBy(1);
        proximidade.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progressChange = 1;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                progressChange = 1 + progress;
                AppData.getInstance().MAX_PROXIMIDADE = progressChange * 0.5;
                proxSeekBar.setText(String.format("%.2f",AppData.getInstance().MAX_PROXIMIDADE));
                Log.d(TAG,String.format("%.2f", AppData.getInstance().MAX_PROXIMIDADE));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;
    }

}
