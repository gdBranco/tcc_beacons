package unb.beacon.beacon_project.Utilidades;

/**
 * Created by samuelpala on 9/5/16.
 */
public class KalmanFilter {
    double Q;
    double R;
    double cov;
    Double x = Double.NaN;
    public KalmanFilter(double R, double Q)
    {
        this.R = R;
        this.Q = Q;
    }
    public double filter(double z)
    {
        if(Double.isNaN(x))
        {
            this.x = z;
            this.cov = Q;
        }
        else
        {
            double predX = (this.x);
            double predCov = this.cov + this.R;

            double K = predCov * 1/(predCov+Q);

            this.x = predX + K *(z-predX);
            this.cov = predCov - (K*predCov);
        }
        return this.x;
    }
    public void Clear(double R, double Q)
    {
        this.R = R;
        this.Q = Q;
        this.x = Double.NaN;
    }
    public double lastMeasure()
    {
        return this.x;
    }
}
