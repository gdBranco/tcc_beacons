\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Problema}{6}{0}{1}
\beamer@subsectionintoc {1}{2}{Objetivos}{9}{0}{1}
\beamer@sectionintoc {2}{Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}{11}{0}{2}
\beamer@subsectionintoc {2}{1}{Pilha TCP/IP}{12}{0}{2}
\beamer@subsectionintoc {2}{2}{Tecnologias para Comunica\IeC {\c c}\IeC {\~a}o Sem Fio}{13}{0}{2}
\beamer@subsubsectionintoc {2}{2}{1}{Bluetooth Low Energy}{15}{0}{2}
\beamer@subsectionintoc {2}{3}{Physical Web}{16}{0}{2}
\beamer@subsubsectionintoc {2}{3}{1}{Beacons}{16}{0}{2}
\beamer@subsubsectionintoc {2}{3}{2}{Eddystone}{17}{0}{2}
\beamer@sectionintoc {3}{Mecanismos para Localiza\IeC {\c c}\IeC {\~a}o de Dispositivos BLE}{18}{0}{3}
\beamer@subsectionintoc {3}{1}{Flutua\IeC {\c c}\IeC {\~a}o do RSSI}{19}{0}{3}
\beamer@subsectionintoc {3}{2}{Modelos de Propaga\IeC {\c c}\IeC {\~a}o do Sinal}{20}{0}{3}
\beamer@subsectionintoc {3}{3}{Filtros}{21}{0}{3}
\beamer@subsectionintoc {3}{4}{Trilatera\IeC {\c c}\IeC {\~a}o}{22}{0}{3}
\beamer@sectionintoc {4}{Resultados}{26}{0}{4}
\beamer@subsectionintoc {4}{1}{Metodologia de Coleta de Informa\IeC {\c c}\IeC {\~o}es}{27}{0}{4}
\beamer@subsectionintoc {4}{2}{Ambientes de Testes}{28}{0}{4}
\beamer@subsectionintoc {4}{3}{An\IeC {\'a}lise dos Filtros}{29}{0}{4}
\beamer@subsectionintoc {4}{4}{Calibra\IeC {\c c}\IeC {\~a}o dos Modelos de Propaga\IeC {\c c}\IeC {\~a}o}{30}{0}{4}
\beamer@subsectionintoc {4}{5}{Calibra\IeC {\c c}\IeC {\~a}o da Converg\IeC {\^e}ncia de RSSI}{31}{0}{4}
\beamer@subsectionintoc {4}{6}{Estimativa de dist\IeC {\^a}ncia}{33}{0}{4}
\beamer@subsectionintoc {4}{7}{Posicionamento}{35}{0}{4}
\beamer@subsectionintoc {4}{8}{Prova de Conceito}{36}{0}{4}
\beamer@sectionintoc {5}{Conclus\IeC {\~a}o}{37}{0}{5}
\beamer@subsectionintoc {5}{1}{Trabalhos Futuros}{39}{0}{5}
\beamer@subsectionintoc {5}{2}{Obrigado}{40}{0}{5}
