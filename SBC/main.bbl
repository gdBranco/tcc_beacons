\begin{thebibliography}{}

\bibitem[Bulten 2015]{2015:Kalman_explained}
Bulten, W. (2015).
\newblock Kalman filters explained: Removing noise from rssi signals.
\newblock
  https://wouterbulten.nl/blog/tech/kalman-filters-explained-removing-noise-from-rssi-signals/,
  Acessado em junho 2016.

\bibitem[Chowdhury et~al. 2015]{PModel:2015}
Chowdhury, T., Rahman, M.~M., Parvez, S.-A., Alam, A., Basher, A., Alam, A.,
  and Rizwan, S. (2015).
\newblock A multi-step approach for rssi-based distance estimation using
  smartphones.
\newblock {\em IEEE}.

\bibitem[Dalce et~al. 2011]{Comparison:2011}
Dalce, R., Val, T., and Bossche, A. V.~D. (2011).
\newblock Comparison of indoor localization systems based on wireless
  communications.
\newblock {\em Wireless Engineering and Technology}.

\bibitem[Galván-Tejada et~al. 2013]{Combined:2013}
Galván-Tejada, C.~E., Carrasco-Jiménez, J.~C., and Brenna, R.~F. (2013).
\newblock Bluetooth-wifi based combined positioning algorithm, implementation
  and experimental evaluation.
\newblock {\em The 2013 Iberoamerican Conference on Electronics Engineering and
  Computer Science}.

\bibitem[Google ]{PhyGoogle}
Google.
\newblock Physical web.
\newblock https://github.com/google/physical-web, acessado em julho 2016.

\bibitem[Google 2016]{Eddystone}
Google (2016).
\newblock Eddystone format.
\newblock https://developers.google.com/beacons/eddystone, Acessado em março
  2016.

\bibitem[KST ]{KST}
KST.
\newblock Kst.
\newblock https://kstechnologies.com/, Acessado em março 2016.

\bibitem[Lindh 2016]{TIReport}
Lindh, J. (2016).
\newblock Bluetooth low energy beacons application report.
\newblock {\em Texas Instruments}.

\bibitem[Tanenbaum and Wheterall 2011]{TanenbaumBook}
Tanenbaum and Wheterall (2011).
\newblock {\em Computer Networks}.
\newblock Pearson.

\bibitem[Townsend et~al. 2014]{BLEBook}
Townsend, K., Cufi, C., Akiba, and Davidson, R. (2014).
\newblock {\em Getting Started with Bluetooth Low Energy: Tools and Techniques
  for Low-Power Networking}.
\newblock O'REILLY.

\bibitem[Vargas 2016]{2016:turco}
Vargas, M.~H. (2016).
\newblock Indoor navigation using bluetooth low energy beacons.
\newblock {\em Turkey University}.

\bibitem[Viswanathan and Srinivasan 2015]{2015:Dfilter}
Viswanathan, S. and Srinivasan, S. (2015).
\newblock Improved path loss prediction model for short range indoor
  positioning using bluetooth low energy.
\newblock {\em IEEE}.

\bibitem[Wang et~al. 2013]{positioning:2013}
Wang, Y., Yang, X., Zhao, Y., Liu, Y., and Cuthbert, L. (2013).
\newblock Bluetooth positioning using rssi and triangulation methods.
\newblock {\em IEEE Consumer Communications and Networking Conference(CCNC)},
  pages 837--842.

\bibitem[Welch and Bishop 2001]{Kalman_intro}
Welch, G. and Bishop, G. (2001).
\newblock An introduction to the kalman filter.
\newblock {\em University of North Carolina at Chapel Hill}.

\end{thebibliography}
