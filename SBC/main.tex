%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How to use writeLaTeX: 
%
% You edit the source code here on the left, and the preview on the
% right shows you the result within a few seconds.
%
% Bookmark this page and share the URL with your co-authors. They can
% edit at the same time!
%
% You can upload figures, bibliographies, custom classes and
% styles using the files menu.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[12pt]{article}
\usepackage{sbc-template}
\usepackage{graphicx,url}
\usepackage[brazil,american]{babel}
\usepackage[utf8]{inputenc}  
\usepackage{indentfirst}
\usepackage{subfigure}
\usepackage{rotating}
\usepackage{float}

\usepackage{color}
\usepackage[table,xcdraw]{xcolor}

\sloppy

\title{Physical Web: Avaliação de Técnicas de Estimativa de Localização de Dispositivos BLE em Ambientes Internos}

\author{Guilherme David Branco, Samuel Vinicius Vieira Pala, Jacir Luiz Bordim}

\address{Departamento de Ciência da Computação,  Universidade de Brasília \\
	Asa Norte,  70910-900, Brasília-DF, Brasil \\
	E-mail\textit{\{gdbranco,samuelpala\}@gmail.com, bordim@unb.br}}
\begin{document} 
	
	\maketitle
	% %------------------------------------------------
	\begin{abstract}
		Physical Web consists in an enabling technology aiming to provide seamless interactions with physical objects and locations, to achieve such a goal the devices are expected to provide acceptable location information. This paper investigates the location information accuracy provided by a BLE device, for this purpose, we have evaluated location accuracy using renowned Received Signal Strength Indicator (RSSI) location-estimation techniques. Besides that, another factor that needs to be accounted for relies in the seamless interactions, needing fast filtering methods to provide a better user experience. Experimental results shows that the BLE beacons used are capable of providing reasonable distance estimation up to $0{,}17$ meters of error, while waiting around $10$ through $15$ seconds.
	\end{abstract}
	
	\begin{resumo}
		Physical Web consiste em uma tecnologia que visa fornecer interações contínuas com objetos físicos e suas respectivas localizações, para atingir o seu objetivo, esses dispositivos devem ser capazes de fornecer informações de localização a um nível aceitável. Este estudo investiga a precisão de informações de localização fornecida por um dispositivo BLE, para isso, avaliou-se sua precisão de localização usando os mecanismos de estimativa de distância baseadas no Indicador de Potência do Sinal (RSSI), além disto, outro fator que deve ser considerado consiste na experiência de uso do sistema, busca-se melhor tempo de resposta para o usuário por meio de técnicas de filtragem rápidas, resultando em esperas pequenas, fazendo com que esta experiência ocorra com maior fluidez. Os resultados experimentais mostram estimativas com erro médio de $0{,}17$m com uma espera entre $10$ a $15$ segundos.
	\end{resumo}
	%------------------------------------------------
	\section{Introdução}
	\textit{Physical Web (PW)} compreende um conceito de tecnologia que visa fornecer interações contínuas com objetos físicos e suas respectivas localizações \cite{PhyGoogle}. Utiliza sinais transmitidos por pequenos dispositivos, chamados de \textit{beacons}, os quais  funcionam como um ``farol'' enviando sinais para que receptores próximos possam identificá-los.  Para empresas, escolas, lojas e outros estabelecimentos, a tecnologia configura uma forma eficiente de comunicação com pessoas que estejam na proximidade do \textit{beacon} emissor e que possam ter interesse em seus anúncios. Por exemplo, uma empresa pode enviar propagandas direcionadas para clientes potenciais conforme ilustrado na Figura~\ref{fig:PWUSO}.
	
	\begin{figure}[tb]
		\includegraphics[width=0.45\textwidth]{./img/pw_uso.jpg}
		\centering
		\caption{Autor Google, Exemplo de aplicacão da PW.}
		\label{fig:PWUSO}
	\end{figure}
	
	Recentemente, a Google disponibilizou um conjunto de protocolos em formato aberto com o intuito de difundir o uso da PW. Para tal, a Google disponibilizou o protocolo Eddystone-URL, o qual define os parâmetros e os formatos para acesso às informações disponibilizadas pelo \textit{beacon} por meio da tecnologia \textit{Bluetooth Low Energy} (BLE) \cite{Eddystone}. O BLE foi criado com o intuito de proporcionar melhor autonomia energética aos dispositivos \cite{TanenbaumBook}. Esta tecnologia tornou possível a utilização de pequenas baterias de $3$V que possibilitam manter os dispositivos BLE ativos por períodos bastante longos se comparados com tecnologias anteriores \cite{TIReport}. O BLE opera em quatro modos distintos: periférico, central, difusão e observador. Na PW, o primeiro e o terceiro modo, que se referem a anunciantes, constituem os principais modos de operação \cite{BLEBook}. Os dispositivos móveis escutam periodicamente o sinal transmitido pelos \textit{beacons} via conexão BLE, este sinal consiste de pequenos pacotes de dados que identificam cada \textit{beacon} e se encontram padronizados por protocolos de encapsulamento, tais como o iBeacon da \textit{Apple} e Eddystone da \textit{Google}.
	
	Para este estudo, o principal emprego da \textit{Physical Web} encontra-se na identificação e localização dos objetos anunciados. Por exemplo, ao receber um anúncio em um centro comercial ou área de lazer, deseja-se poder distinguir entre os anunciantes e seus produtos. Isto deve ser feito de maneira fluída ao usuário, com rapidez na convergência de estimativa de distância de forma a permitir identificar e filtrar os objetos por sua proximidade. Os principais desafios para a identificação de produtos e anunciantes recaem na necessidade de estimar a distância do receptor e do \textit{beacon}. Realiza-se a estimativa por meio do Indicador de Potência do Sinal (RSSI), entretanto, este pode variar significativamente, especialmente em ambientes fechados \cite{PModel:2015}. Este trabalho tem como foco verificar a acurácia das técnicas de estimativa de distância para dispositivos BLE que utilizam o RSSI. Além disso, avalia-se o tempo e o quantitativo de leituras necessárias para se obter uma estimativa de distância aproximada. Os resultados experimentais mostraram estimativas com erro médio de $0{,}17$m e um tempo de convergência entre $10$ a $15$ segundos.
	
	Para este estudo, apresenta $3$ seções principais: a Seção \ref{mec} mostra uma breve revisão dos modelos de propagação e técnicas de filtragem. A Seção \ref{expenv}, apresenta o cenário de experimentos e a Seção \ref{result}, os resultados dos experimentos laboratoriais sobre a estimativa de distância e melhoria da fluidez de utilização por meio da calibragem do tempo de espera da convergência das estimativas. Por fim, a Seção \ref{concl} apresenta a conclusão. 
	%------------------------------------------------
	\section{Mecanismos de Localização} \label{mec}
	
	Devido a natureza simples dos dispositivos BLE, os mecanismos de localização empregados se resumem na utilização do RSSI como forma de estimativa de distância e posicionamento. Esta seção apresenta uma breve revisão dos modelos de propagação  mais utilizados na literatura bem como os filtros comumente utilizado para amenizar flutuações nas leituras realizadas. 
	
	\subsection{Modelos de Propagação}
	Dentre os modelos de propagação existentes, destacam-se  o  \textit{Log Distance Path Loss Model} (LDPLM) e \textit{Rappaport's Path Loss Model} (RPLM) \cite{Combined:2013,positioning:2013,Comparison:2011}. 
	
	O LDPLM, consiste em um modelo padrão para desvanecimento de potência de sinal para antenas sem controle de direção ou conhecimento de ganho da antena. Este modelo se mostra amplamente utilizado em ambientes internos e se define pela seguinte equação:
	\begin{equation}
	RSSI = RSSI_{0} - 10*n*\log_{10}(d), 
	\end{equation}
	onde $RSSI$ se refere ao valor estimado para RSSI, $RSSI_{0}$ o valor para o RSSI a $1$m de distância do transmissor, $n$ coeficiente de perda de percurso, para ambientes livres $n = 2$, e $d$ a distância.
	Para calcular a distância basta isolar $d$:
	\begin{equation}
	d = 10^{\frac{RSSI_{0} - RSSI}{10*n}}.
	\end{equation}
	
	O modelo de propagação de Rapapport consiste uma evolução ao modelo de propagação LDPLM e visa incluir o fenômeno \textit{Slow-fading}, que ocorre devido a obstruções no caminho \cite{2016:turco}. O termo $X$, assim como $n$, coeficiente de perda de percurso, objetiva dar melhor controle ao modelo, em geral, utiliza-se a variância como valor para o termo $X$, conforme definido abaixo:
	\begin{equation}
	RSSI = RSSI_{0} - 10*n*\log_{10}(d) + X.
	\end{equation}
	Para calcular a distância basta isolar $d$, assim como no modelo anterior:
	\begin{equation}
	d = 10^{\frac{RSSI_{0} - RSSI + X}{10*n}}. \label{eq:rappa}
	\end{equation}
	
	\subsection{Filtros}
	Devido a inconsistência entre as leituras no dispositivo receptor causadas por fatores externos, como sombreamento, multipercurso, desvanecimento do sinal entre outros, necessita-se utilizar técnicas de filtragem, que objetivam atenuar as flutuações do sinal. 
	Dentre as técnicas existentes, destacam-se Média Móvel (MM), Filtro de Kalman (K) \cite{Kalman_intro} e Filtro Duplo (do Inglês, \textit{Double Filter} (DF)) \cite{2015:Dfilter}.
	
	A MM utiliza o conceito de média simples somado à movimentação de uma janela de dados, assim ajustando o tamanho da janela pode-se fazer uma média de acordo com a chegada de valores. Portanto:
	\begin{equation}
	M = \frac{\sum{a_i}}{k},
	\end{equation}
	onde $k$ representa o tamanho da janela, e $i$ vai de 1 a $k$.
	
	O filtro de Kalman se baseia em um estimador de estados que presume o valor de uma variável a partir de entradas com ruído, expressado por um algoritmo que se fundamenta no histórico das entradas. Além disso, o filtro de Kalman assume um modelo linear, portanto os estados e as estimativas devem ser transformações lineares. Define-se o estado $x_{t}$ como uma combinação do estado anterior $x_{t-1}$ dado uma matriz de transformação $A$, um controle $u_{t}$ junto também de uma matriz de transformação $B$ e o ruído processado, proveniente do próprio sistema $E$. Seguiu-se a ideia \cite{2015:Kalman_explained}, de desconsiderar a variável de controle $u$ e utilizar $A$ como matriz identidade, tornando o modelo mais simples:
	\begin{equation}
	x_{t} = A_{t}x_{t-1} + B_{t}u_{t} + E_{t} \approx x_{t-1} + E_{t}. 
	\end{equation}
	Faz-se necessária a definição de como um estado $X$ resulta em uma medição $z$.
	\begin{equation}
	Z_{t} = C_{t}x_{t} + \delta_{t},
	\end{equation}
	onde C representa a matriz de transformação e $\delta$ o ruído.	A etapa de atualização do filtro ocorre pela predição, da seguinte forma:
	\begin{equation}
	\overline{\mu_{t}}= \mu_{t-1}
	\end{equation}
	\begin{equation}
	\overline{\Sigma_{t}} = \Sigma_{t-1} + R_{t}.
	\end{equation}
	Note que $\mu$ denota a predição e $X$ o estado real, já $\Sigma$ define a certeza da predição e $R$ o ruído causado pelo sistema, pode-se assumir que devido ao ambiente estático o ruído esperado procede, quase exclusivamente, do ambiente. Em ambiente dinâmico, onde há o movimento do receptor, deve-se aumentar o valor desta variável.	A partir da etapa de predição computa-se o ganho de Kalman:
	\begin{equation}
	K_{t} = \overline{\Sigma_{t}}(\overline{\Sigma_{t}}Q_{t})^{-1}.
	\end{equation}
	Onde $Q$ representa o ruído da informação. Para modelagem sobre RSSI, a variância simboliza o valor de influência. Assim, o ganho juntamente a $Q$ serve como peso para avaliar a certeza da predição contra a certeza da medição. O que torna a etapa de atualização completa por:
	\begin{equation}
	\mu _{t}= \overline{\mu_{t}} + K_{t}(z_{t} - \overline{\mu_{t}})
	\end{equation}
	\begin{equation}
	\Sigma_{t} = \overline{\Sigma_{t}} - (K_{t}\overline{\Sigma_{t}}),
	\end{equation}
	desta forma, quanto maior o ganho $K$ mais a medição se torna integrada ao sistema, e quanto menor este ganho mais a predição se torna integrada. 
	
	O \textit{Double Filter} caracteriza-se pelo uso de duas variáveis de controle $A$ e $B$ \cite{2015:Dfilter}. A variável $A$ controla o ganho do sinal, sendo um valor de $0{,}5$ a $1$, simbolizando as porcentagens de ganho, enquanto $B$ representa um valor de controle para o erro propagado no sistema, variando de $0{,}05$ a $0{,}5$, conforme definido abaixo:
	\begin{equation}
	S_{n} = (1-A_{n})S_{n-1} + A_{n}(E(S_{n}) + (RSSI_{n} - E(S_{n}) B_{n}) 
	\end{equation}
	Onde $S_{n}$ simboliza o novo valor do sinal filtrado, $S_{n-1}$, o valor anterior do sinal filtrado e $E(S_{n})$, o valor de erro do sistema até o momento calculado através de uma média simples dos valores de RSSI filtrados de $0$ a $n-1$. $A$ e $B$ constituem vetores atualizados por:
	\begin{equation}
	Se ( (A_{n} > A_{B}) \&\& (B_{n} > B_{b}) ) := \\
	A_{n} = W_{n}A_{n-1}, B_{n} = W_{n}B_{b-1}
	\end{equation}
	Onde $A_{B}$, $B_{B}$ e $W_{n}$ representam valores de limitação, dados como $0{,}7$, $0{,}05$ e $0{,}9$, respectivamente, de acordo com o estudo.
	
	%------------------------------------------------
	\section{Ambiente Experimental} \label{expenv}
	Esta seção apresenta a metodologia utilizada para a coleta de dados, bem como o \textit{hardware} e o \textit{software} necessários para obter-se às informações desejadas. Para avaliação da estimativa de distância com base no RSSI dos dispositivos \textit{beacon} BLE, escolheu-se dispositivos que oferecessem suporte às tecnologias necessárias ao estudo. Ressalta-se que durante este estudo ainda não existiam \textit{beacons} oferecidos no mercado nacional, assim, optou-se pela importação de dois modelos de \textit{beacons}: o Ion e Particle, ambos da empresa KST \cite{KST}. Como dispositivo receptor, utilizou-se um \textit{smartphone} que atendesse as especificações quanto à versão \textit{Bluetooth} (BLE) e acesso à última API \textit{Android} compatível com o Eddystone \cite{Eddystone}, Lenovo Moto-Z XT$1635$-$02$. Para a coleta das informações, desenvolveu-se uma aplicação para o \textit{Android} que permitisse obter as informações do \textit{beacon} bem como estimar a potência do sinal (RSSI). 
	
	 O \textit{beacon} envia mensagens encapsuladas com protocolo Eddystone por meio do sinal BLE, capturado pelo dispositivo móvel na interface \textit{Android}, passando pelos módulos \textit{parser}, filtragem e estimativa de distância e finalizando com a atualização da interface, como ilustrado na Figura \ref{fig:APP_FLUX}.
	\begin{itemize}
		\item \textbf{\textit{Parser}}: 
		A informação passa por um \textit{parser} onde aplica-se uma máscara de \textit{bits}, para a extração das informações necessárias, a \textit{Identification} e a \textit{Namespace}. Já, o RSSI e o \textit{Power} possuem métodos para sua obtenção por meio da API \textit{Android}.
		\item \textbf{Filtragem }: 
		O RSSI passa por filtros com o intuito de reduzir o ruído do sinal.
		\item \textbf{Estimativa de distância}:  
		A partir da filtragem, calcula-se a distância com base no modelo RPLM, pois mostrou-se melhor em relação aos demais.
	\end{itemize}

	\begin{figure}[tb]
		\centering
		
		\subfigure[Fluxograma do sistema]{
			\label{fig:APP_FLUX}
			\includegraphics[width=0.45\textwidth]{./img/flux-real.pdf}
		}
		\subfigure[Distância máxima devido ao desvanecimento do sinal]{
			\label{fig:max_dist}
			\includegraphics[width=0.45\textwidth]{./graph/6m_comp.pdf}
		}

		\caption{Distância Máxima e Fluxograma}
	\end{figure}
	
	Os testes iniciais foram projetados com base nos problemas experienciados nos fundamentos e nos artigos abordados nas seções anteriores. Em todo os experimentos seguiu-se um padrão, sendo realizadas ao menos $25$ leituras, e os que utilizam distância, começa-se em $1$m e termina-se em $6$m. Os valores de $6$ metros em diante não se revelaram tão úteis quanto valores anteriores a esta marca, como demonstrado na Figura \ref{fig:max_dist}, devido a natureza dos dispositivos transmissores e receptores, que para manter um consumo energético menor, seu \textit{hardware}, no caso, as antenas, possuem menor capacidade de operação, não distinguindo eficientemente as variações que ocorrem após os $5$m, resultando em maior erro após esse limite. Ainda na Figura \ref{fig:max_dist}, pode-se observar um padrão onde há um desvanecimento da intensidade do sinal à medida que se aumenta a distância. Fato que não ocorre do quinto metro em diante, limitando a estimativa útil até os $5$m. \textcolor{black}{As opções de configuração de tempo para envio de anúncios não surtiram efeitos, posto que a mudança do intervalo de transmissão de $1$s para $100$ms não gerou leituras proporcionais, mantendo-se em torno dos $1{,}2$s por leitura. Isso pode ocorrer devido a falha no \textit{hardware} dos \textit{beacons} ou à falta de opção de configuração da taxa de leitura na API \textit{Android} para BLE. } \textcolor{black}{Duas áreas para os testes foram selecionadas, ambas na Universidade de Brasília (UnB), uma com bom campo de visão, situada atrás do departamento de Ciência da Computação e outra no laboratório COMNET.  As Figuras \ref{fig:area} e \ref{fig:dep} denotam os locais dos testes.}
	
	\begin{figure}[tb]
		\centering
		\subfigure[Atrás do departamento]{
			\label{fig:area}
			\includegraphics[width=0.35\textwidth]{./img/dist_4m.jpg}
		}
		\subfigure[Ambiente Lab. COMNET]{
			\label{fig:dep}
			\includegraphics[width=0.35\textwidth]{./img/lab.png}
		}
		\caption{Experimento de desvanecimento de RSSI e estimativa de distância}
	\end{figure}
	
	\section{Resultados} \label{result}
	Nesta seção, apresenta-se os resultados referentes à estimativa de distância, utilizando-se das técnicas explicadas na seção anterior. Em particular, esta seção visa mensurar a acurácia da estimativa e o tempo de convergência, a fim de serem constatados os valores que possibilitem o desenvolvimento de aplicações baseadas em estimativa de distância a partir do RSSI com a tecnologia BLE.
	\subsection{Flutuação da Intensidade do Sinal} \label{Flut}
	O indicador de potência de sinal, RSSI, apresentou grande variação ao longo do experimento, mesmo em ambiente estático, sem movimento da fonte ou do receptor. Para um \textit{beacon} colocado a um metro e aumentando a distância a cada metro do receptor, sendo realizadas $25$ leituras por metro, pôde-se observar variância média de $7{,}43$dB para o modelo Ion. Na Figura \ref{fig:Rssi_filtros}, observa-se a oscilação entre as leituras, o que evidencia a necessidade de técnicas de filtragem.
	
	O valor de RSSI foi utilizado como forma de estimativa de distância, portanto, a filtragem tornou-se necessária devido a varição no sinal, causada por efeitos físicos como reflexo do sinal, multipercurso, sombreamento, desvanecimento da intensidade entre outros.
	
	\label{FiltrosTrue}
	Com relação a filtragem pela Média Móvel, tornou-se possível obter a média de acordo com as leituras, o tamanho adotado para a janela foi de $25$, pois necessita-se de ao menos $20$ para garantir confiabilidade a informação. Nota-se diminuição da variância, porém a convergência para novos valores se torna mais lenta.
	
	Já, para o Filtro de Kalman, como explicado anteriormente, notou-se menor discrepância entre os valores de leitura, o que resultou em melhor precisão no recebimento de informação referente ao RSSI e por consequência na estimativa de distância.
	
	Por fim, implementou-se uma versão do filtro duplo, porém os resultados não se mostraram interessantes em ambientes dinâmicos quando comparados com ambientes estáticos, pois o filtro em questão depende dos primeiros valores para convergir a predição, além disso se mostra resistente a variações bruscas no sinal, raramente divergindo de sua predição inicial.
	
	Os resultados dos três filtros podem ser constatados na Figura \ref{fig:Rssi_filtros} e nota-se grande vantagem em relação à variação não filtrada. A média móvel se apresenta melhor que o filtro de Kalman, porém manifesta um problema semelhante ao filtro Duplo, no qual se faz necessário maior número de leituras para convergir a um novo valor de RSSI, piorando a precisão em ambientes dinâmicos.
	
	\begin{figure}[tb]
		\includegraphics[width=0.45\textwidth]{./graph/rssi_1m.pdf}
		\centering
		\caption{RSSI ao longo do tempo observado a 1m de distância}
		\label{fig:Rssi_filtros}
	\end{figure}
	
	\subsection{Calibração dos Modelos de Propagação}
	Para a calibração do modelo LDPLM e RPLM, necessitou-se encontrar o valor de $RSSI_0$, para tal realizou-se leituras e obteve-se a média, sendo esta de $-57{,}70$dB no modelo Ion e $-53{,}20$dB no modelo Particle. Já para o valor $X$, utilizou-se a média das variâncias para o filtro de Kalman, obtendo-se $1{,}22$dB. Por fim, para o coeficiente de perda do sinal $n$, obteve-se, empiricamente, o valor de $2{,}55$. O cálculo da distância para estes parâmetros encontram-se na Tabela \ref{tab:modelos-comp2}, o modelo de Friis não se comportaria bem para os ambientes internos.
	
	\begin{table}[tb]
		\centering
		\scriptsize
		\caption{Comparação entre os modelos LDPLM e  RPLM}
		\label{tab:modelos-comp2}
		\begin{tabular}{c|c|c|c}
			\hline RSSI (dB)  &  LDPLM (m) & RPLM (m) & Distância Real (m) \\ \hline \hline
			$-57{,}64$ & $0{,}99$      & $1{,}11$     & $1{,}00$               \\
			$-64{,}20$ &  $1{,}80$      & $2{,}00$     & $2{,}00$               \\
			$-69{,}40$ &  $2{,}88$      & $3{,}21$     & $3{,}00$               \\
			$-70{,}96$ &  $3{,}31$      & $3{,}69$     & $4{,}00$               \\
			$-75{,}04$ &  $4{,}79$      & $5{,}33$     & $5{,}00$               \\ \hline \hline
			Erro Médio (m)& $0{,}24$		& $0{,}19$	   & $0{,}00$
		\end{tabular}
	\end{table}
	
	\subsection{Calibração da Convergência de RSSI}
	Um problema enfrentado pelo usuário, ao utilizar aplicações que contenham técnicas de filtragem, ocorre pela demora na convergência dos valores de RSSI, posto que se esse tempo for muito grande, a pessoa perderá o interesse no \textit{software} e recorrerá a outros métodos para a localização do objeto físico.
	
	Modificando os valores iniciais do filtro de Kalman, pôde-se chegar, rapidamente, à convergência, porém isto causa efeitos colaterais na variância como consta na Figura \ref{fig:3kalmans}. Portanto, optou-se por utilizar uma convergência em equilíbrio com o seu efeito colateral, para que fosse possível obter um novo valor de convergência, sem afetar de forma desfavorável a estimativa de distância, sendo estes valores $Q = 1{,}5$ e $R=0{,}7$, como expressado na Figura \ref{fig:conv2}. Os valores convergiram por volta de $15$ leituras, contra mais de $25$ do modelo inicial, onde cada leitura ocorreu em torno de $1{,}2$s. 
	
	O filtro Duplo e a Média Móvel possuem grande rigidez na convergência, por isso garantem alta precisão na estimativa, porém voltam para o problema da espera do usuário. O filtro Duplo não reagiu conforme o estudo original \cite{2015:Dfilter} mesmo modificando os valores de controle, mas se manteve bom em ambientes estáticos. Já, a Média Móvel necessita de uma espera do tamanho de sua janela em questão de quantidade de leituras.
	
	\begin{figure}[tb]
		\centering
		\subfigure[Variância no filtro Kalman devido a mudança de valores iniciais]{
			\label{fig:3kalmans}
			\includegraphics[width=0.45\textwidth]{./graph/3k_compare.pdf}
		}
		\subfigure[Quantidade de leituras para convergência nos filtros]{
			\label{fig:conv2}
			\includegraphics[width=0.45\textwidth]{./graph/conv2.pdf}
		}
		\caption{Variância e convergência no filtro de Kalman}
	\end{figure}
	
	\subsection{Estimativa de Distância}
	A partir dos resultados com filtragem sobre o valor do RSSI, pôde-se estimar a distância de maneira mais confiável devido a baixa flutuação de valores e melhor convergência. Para tal, utilizou-se a estimação pelo método de \textit{Rappaport} de desvanecimento do sinal a partir do isolamento da variável de distância como explicado previamente.
	
	O melhor erro médio foi de $0{,}12$m para o Filtro Duplo no modelo Ion de \textit{beacon}, valor aceitável quando comparado a erros encontrados na literatura. Porém, o Filtro Duplo, assim como a Média Móvel possuem alta rigidez quanto a convergência para novos valores. A partir do erro de estimativa da distância, calculou-se a taxa de melhora em relação ao teste Sem Filtro (SF), onde a Média Móvel obteve $18$\%, Kalman $11$\% e o Filtro Duplo $58$\%.
	
	Considerando somente a Tabela \ref{tab:distanciaIon}, pode-se inferir que o valor sem filtragem foi apropriado, porém, vale lembrar que os valores apresentados passaram por uma média aritmética devido a variação de seus índices, o que faz com que valores sem filtragem se comportem de maneira similar a Média Móvel, a variação se torna evidente na Tabela \ref{tab:dist_var}, obteve-se média da variância de $7,43$db num cenário sem filtragem.
	
	O filtro de Kalman se mostrou inferior em relação aos outros, mas com erro ainda satisfatório e, além disso, possui bom controle sobre a flexibilidade da filtragem, podendo assim se tornar mais rígido com valores de estimativa semelhantes a seus concorrentes. Ao se considerar a movimentação do usuário, mostrou-se com melhor convergência, resultando em maior confiabilidade da estimativa, pois os demais não acompanharam os novos valores de RSSI, aumentando os seus erros gradativamente ao movimento.
	
	\begin{table}[tb]
		\centering
		\scriptsize
		\caption{Estimativa de distância - Ion}
		\label{tab:distanciaIon}
		\begin{tabular}{c|c|c|c|c}	
			\hline \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}Distância \\ Real (m)\end{tabular}} & Sem Filtro & Média Móvel & Kalman & Fltro Duplo \\ \hline \hline
			\multicolumn{1}{c|}{$1,00$}     		& $1,11$ & $1,11$ & $1,10$ & $1,10$ \\ 
			\multicolumn{1}{c|}{$2,00$}     		& $2,00$ & $1,83$ & $1,90$ & $1,74$ \\ 
			\multicolumn{1}{c|}{$3,00$}     		& $3,21$ & $3,06$ & $3,08$ & $2,81$ \\ 
			\multicolumn{1}{c|}{$4,00$}     		& $3,69$ & $3,73$ & $3,70$ & $4,00$ \\ 
			\multicolumn{1}{c|}{$5,00$}     		& $5,33$ & $5,21$ & $5,30$ & $4,92$ \\ \hline \hline
			\multicolumn{1}{c|}{Erro Médio (m)}     & $0,19$ & $0,16$ & $0,17$ & $0,12$ \\  
		\end{tabular}
	\end{table}
	
	\begin{table}[tb]
		\centering
		\scriptsize
		\caption{Variação e desvio padrão para cada metro e filtragem}
		\label{tab:dist_var}
		\begin{tabular}{c c|c|c|c|c|c|c|c}
			
			\hline & \multicolumn{8}{c}{Ion}                                            \\ \cline{2-9} \cline{2-9}
			& \multicolumn{4}{c|}{Variância} & \multicolumn{4}{c}{Desvio Padrão} \\ \hline \hline
			\multicolumn{1}{c|}{Distância (m)} & SF     & MM    & K     & DF    & SF      & MM     & K      & DF     \\ \hline \hline
			\multicolumn{1}{c|}{$1{,}00$}             &$ 4{,}24   $&$ 0{,}08  $&$ 0{,}20  $&$ 0{,}03  $&$ 2{,}06    $&$ 0{,}29   $&$ 0{,}44   $&$ 0{,}17 $  \\ 
			\multicolumn{1}{c|}{$2{,}00$}             &$ 12{,}50  $&$ 0{,}57  $&$ 1{,}96  $&$ 0{,}07  $&$ 3{,}54    $&$ 0{,}75   $&$ 1{,}40   $&$ 0{,}26   $\\ 
			\multicolumn{1}{c|}{$3{,}00$}             &$ 8{,}67   $&$ 2{,}67  $&$ 3{,}04  $&$ 0{,}25  $&$ 2{,}94    $&$ 1{,}63   $&$ 1{,}74   $&$ 0{,}42$   \\ 
			\multicolumn{1}{c|}{$4{,}00$}             &$ 5{,}37   $&$ 0{,}15  $&$ 0{,}29  $&$ 0{,}38  $&$ 2{,}32    $&$ 0{,}39   $&$ 0{,}54   $&$ 0{,}62$   \\ 
			\multicolumn{1}{c|}{$5{,}00$}             &$ 6{,}37   $&$ 0{,}34  $&$ 0{,}61  $&$ 0{,}05  $&$ 2{,}52    $&$ 0{,}58   $&$ 0{,}78   $&$ 0{,}22 $  \\ \hline \hline
			\multicolumn{1}{c|}{Média}         &$ 7{,}43   $&$ 0{,}76  $&$ 1{,}22  $&$ 0{,}15  $&$ 2{,}68    $&$ 0{,}73   $&$ 0{,}98   $&$ 0{,}34   $\\
		\end{tabular}
	\end{table}

%	\textcolor{red}{\textbf{Caso de uso} \\
	Realizou-se um experimento real em que o usuário recebe informações sobre produtos de um \textit{Food Truck} fictício com base na proximidade ao \textit{beacon}. O resultado obtido está em consonância com o apresentado nas seções anteriores. As Figuras \ref{fig:app1} e \ref{fig:app2} ilustram o sistema do caso de uso onde o usuário recebe informações do cardápio em conformidade com a sua proximidade do \emph{beacon}. 
	
	
	
	\begin{figure}[tb]
		\centering
		\subfigure[Encontrando o sinal e estimativa de distância de \textit{beacons} próximos]{
			\label{fig:app1}
			\includegraphics[width=0.45\textwidth]{./img/app2.png}
		}
		\subfigure[Identificando o sinal e mostrando os produtos de um \textit{Food Truck} em proximidade]{
			\label{fig:app2}
			\includegraphics[width=0.45\textwidth]{./img/app1.png}
		}
		\caption{Sistema recebendo informações sobre os produtos de um \textit{Food Truck}}
	\end{figure}
		
	%------------------------------------------------
	\section{Conclusão} \label{concl}
	Com os resultados aqui apresentados, obtendo-se erro médio de $0{,}17$m para a estimativa de distância e em torno de $9$ leituras para convergência, argumenta-se ser a intensidade do sinal uma boa opção para localização para ambientes internos, com acurácia relativamente alta, condizendo, pois, com outros estudos já desenvolvidos nesse cenário. Como trabalho futuro, sugere-se a utilização de outras funcionalidades da API \textit{Android}, como giroscópio e acelerômetro, para modificar os valores de controle dos filtros em tempo real de acordo com a movimentação. Assim, após se obter uma estimativa satisfatória e o usuário permanecer parado, pode-se aumentar a precisão filtrando-se os valores de maneira mais rígida, e aumentando-se a flexibilidade quando o usuário se mover.
	%------------------------------------------------
	
	\bibliography{bibliografia}
	\bibliographystyle{sbc}
	%------------------------------------------------
	
\end{document}